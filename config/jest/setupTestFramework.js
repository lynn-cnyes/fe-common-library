/* global jasmine:false */
/* eslint-disable global-require */
import 'jest-enzyme';

if (process.env.CI) {
  const jasmineReporters = require('jasmine-reporters');
  const junitReporter = new jasmineReporters.JUnitXmlReporter({
    savePath: 'testresults',
    consolidateAll: false,
  });

  jasmine.getEnv().addReporter(junitReporter);
}

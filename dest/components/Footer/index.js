(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "classnames"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react"), require("classnames")) : factory(root["prop-types"], root["react"], root["classnames"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_6__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 133);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 133:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(50);


/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(6);

var _classnames2 = _interopRequireDefault(_classnames);

var _getStyleName = __webpack_require__(4);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _Footer = __webpack_require__(51);

var _Footer2 = _interopRequireDefault(_Footer);

var _links = __webpack_require__(52);

var _links2 = _interopRequireDefault(_links);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */


var Footer = function (_PureComponent) {
  _inherits(Footer, _PureComponent);

  function Footer() {
    _classCallCheck(this, Footer);

    return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).apply(this, arguments));
  }

  _createClass(Footer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          now = _props.now,
          Link = _props.Link;

      var thisYear = new Date(now).getFullYear();

      /* eslint-disable jsx-a11y/accessible-emoji */
      return _react2.default.createElement(
        'div',
        { id: (0, _getStyleName2.default)(_Footer2.default, 'cnyes-footer-wrapper'), className: (0, _classnames2.default)('theme-footer-wrapper') },
        _react2.default.createElement(
          'footer',
          { className: (0, _getStyleName2.default)(_Footer2.default, 'main-footer') },
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'logo') },
            Link ? _react2.default.createElement(Link, { to: '/' }) : _react2.default.createElement('a', { href: '/' })
          ),
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'nav') },
            _react2.default.createElement(
              'nav',
              null,
              this.constructor.renderNavs()
            ),
            _react2.default.createElement(
              'small',
              { className: (0, _getStyleName2.default)(_Footer2.default, 'copyright-anue') },
              '\xA9 Copyright 2000-',
              thisYear,
              ' anue.com All rights reserved. \u672A\u7D93\u6388\u6B0A \u4E0D\u5F97\u8F49\u8F09'
            )
          ),
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'socials') },
            this.constructor.renderSocials()
          )
        )
      );
    }
  }], [{
    key: 'renderNavs',
    value: function renderNavs() {
      return _links2.default.navs.map(function (item) {
        return _react2.default.createElement(
          'a',
          { href: item.url, key: 'footer-nav-' + item.name, target: '_blank', rel: 'noopener noreferrer' },
          item.title
        );
      });
    }
  }, {
    key: 'renderSocials',
    value: function renderSocials() {
      return _links2.default.socials.map(function (item) {
        return _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Footer2.default, 'social-item'), key: 'footer-socials-' + item.name },
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'title') },
            item.title
          ),
          _react2.default.createElement(
            'a',
            {
              className: (0, _getStyleName2.default)(_Footer2.default, 'social-icon') + ' ' + (0, _getStyleName2.default)(_Footer2.default, 'cnyes-media-' + item.name),
              href: item.url,
              rel: 'noopener noreferrer',
              target: '_blank'
            },
            item.title
          )
        );
      });
    }
  }]);

  return Footer;
}(_react.PureComponent);

Footer.propTypes = {
  now: _propTypes2.default.number.isRequired,
  Link: _propTypes2.default.func
};
Footer.defaultProps = {
  Link: undefined
};
exports.default = Footer;

/***/ }),

/***/ 51:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cnyes-footer-wrapper":"_qdV4M","main-footer":"_1B2qL","logo":"_1W3uT","nav":"_36Xgk","socials":"_7vG5K","social-item":"_1n8HS","title":"_2C4rE","social-icon":"_11SpT","cnyes-media-facebook":"_2WAgR","cnyes-media-line":"_1PYxM","cnyes-media-app":"_13are","copyright-anue":"_P9DLg"};

/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var footerLinks = {
  navs: [{
    title: '關於我們',
    name: 'about',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_about.html'
  }, {
    title: '集團簡介',
    name: 'anueCorp',
    url: 'https://www.anuegroup.com.tw/'
  }, {
    title: '廣告服務',
    name: 'ad',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_AD01.html'
  }, {
    title: '金融資訊元件',
    name: 'financial',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_pas01.html'
  }, {
    title: '聯絡我們',
    name: 'contact',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_ctcUsTpe.html'
  }, {
    title: '徵才',
    name: 'job',
    url: 'http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=5e6042253446402330683b1d1d1d1d5f2443a363189j01'
  }, {
    title: '網站地圖',
    name: 'sitemap',
    url: 'http://www.cnyes.com/cnyes_about/site_map.html'
  }, {
    title: '法律聲明',
    name: 'legal',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_sos01.html'
  }],
  socials: [{
    title: '粉 絲 團',
    name: 'facebook',
    url: 'https://www.facebook.com/anuetw/'
  }, {
    title: '鉅亨網Line',
    name: 'line',
    url: 'https://line.me/ti/p/@ZLU0489G'
  }, {
    title: '鉅亨網APP',
    name: 'app',
    url: 'http://www.cnyes.com/app?utm_source=cnyes&utm_medium=desktop&utm_campaign=desktop_footer'
  }]
};

exports.default = footerLinks;

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ })

/******/ });
});
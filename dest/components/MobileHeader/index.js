(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "classnames"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react"), require("classnames")) : factory(root["prop-types"], root["react"], root["classnames"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_6__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 134);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(135);


/***/ }),

/***/ 135:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MobileNavBoard = exports.MobileMenu = undefined;

var _MobileMenu = __webpack_require__(53);

var _MobileMenu2 = _interopRequireDefault(_MobileMenu);

var _NavBoard = __webpack_require__(55);

var _NavBoard2 = _interopRequireDefault(_NavBoard);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.MobileMenu = _MobileMenu2.default;
exports.MobileNavBoard = _NavBoard2.default;

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 53:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(6);

var _classnames2 = _interopRequireDefault(_classnames);

var _MobileMenu = __webpack_require__(54);

var _MobileMenu2 = _interopRequireDefault(_MobileMenu);

var _getStyleName = __webpack_require__(4);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function MobileMenu(_ref) {
  var showCatBoard = _ref.showCatBoard,
      channelName = _ref.channelName,
      hideTopBar = _ref.hideTopBar;

  return _react2.default.createElement(
    'div',
    { className: (0, _classnames2.default)(_defineProperty({}, (0, _getStyleName2.default)(_MobileMenu2.default, 'index-header-top-bar'), !hideTopBar)) },
    _react2.default.createElement(
      'a',
      { href: 'https://m.cnyes.com/news', className: _MobileMenu2.default['index-header-logo'] },
      '\u9245\u4EA8\u7DB2'
    ),
    channelName && channelName.length && _react2.default.createElement(
      'div',
      { className: _MobileMenu2.default['header-channel-label'] },
      channelName
    ),
    _react2.default.createElement(
      'nav',
      null,
      _react2.default.createElement('span', { className: _MobileMenu2.default['index-header-menu'], onClick: showCatBoard })
    )
  );
}

MobileMenu.propTypes = {
  showCatBoard: _propTypes2.default.func.isRequired,
  channelName: _propTypes2.default.string,
  hideTopBar: _propTypes2.default.bool
};

MobileMenu.defaultProps = {
  channelName: undefined,
  hideTopBar: false
};

exports.default = MobileMenu;

/***/ }),

/***/ 54:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"index-header-top-bar":"_3Ny_k","index-header":"_27f3x","index-header-menu":"_3Wta4","index-header-logo":"_3KhXq","header-channel-label":"_3_hO3","index-header-tabs":"_3vTaA","index-search-hint":"_3ZBZ7","hint-close":"_3XvKH","index-header-search":"_2mXhz"};

/***/ }),

/***/ 55:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(6);

var _classnames2 = _interopRequireDefault(_classnames);

var _NavItem = __webpack_require__(56);

var _NavItem2 = _interopRequireDefault(_NavItem);

var _navConfig = __webpack_require__(58);

var _navConfig2 = _interopRequireDefault(_navConfig);

var _NavBoard = __webpack_require__(59);

var _NavBoard2 = _interopRequireDefault(_NavBoard);

var _getStyleName = __webpack_require__(4);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NavBoard = function (_PureComponent) {
  _inherits(NavBoard, _PureComponent);

  function NavBoard() {
    _classCallCheck(this, NavBoard);

    return _possibleConstructorReturn(this, (NavBoard.__proto__ || Object.getPrototypeOf(NavBoard)).apply(this, arguments));
  }

  _createClass(NavBoard, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          hideTopBar = _props.hideTopBar,
          isCatBoardOpen = _props.isCatBoardOpen,
          hideCatBoard = _props.hideCatBoard;

      var catBoardClassName = isCatBoardOpen ? _NavBoard2.default['nav-board-show'] : _NavBoard2.default['nav-board'];

      return _react2.default.createElement(
        'aside',
        {
          className: (0, _classnames2.default)(catBoardClassName, 'theme-gradient', _defineProperty({}, (0, _getStyleName2.default)(_NavBoard2.default, 'nav-board-top-bar'), !hideTopBar))
        },
        _react2.default.createElement(
          'header',
          null,
          _react2.default.createElement(
            'span',
            { className: _NavBoard2.default['nav-board-logo'] },
            '\u9245\u4EA8\u7DB2'
          ),
          _react2.default.createElement('button', { className: _NavBoard2.default['nav-board-btn-close'], onClick: hideCatBoard })
        ),
        _react2.default.createElement(
          'main',
          null,
          _navConfig2.default.map(function (nav) {
            return [_react2.default.createElement(
              'h4',
              { key: nav.name + '-h4', className: 'nav-board-subtitle theme-nav-board-subtitle' },
              nav.title
            ), _react2.default.createElement(
              'nav',
              { key: nav.name + '-nav', className: _NavBoard2.default['cat-board-nav'] },
              nav.items && nav.items.map(function (item) {
                return _react2.default.createElement(_NavItem2.default, { key: item.name, navTitle: nav.title, item: item });
              })
            )];
          })
        )
      );
    }
  }]);

  return NavBoard;
}(_react.PureComponent);

NavBoard.propTypes = {
  isCatBoardOpen: _propTypes2.default.bool,
  hideCatBoard: _propTypes2.default.func,
  hideTopBar: _propTypes2.default.bool
};
NavBoard.defaultProps = {
  isCatBoardOpen: false,
  hideCatBoard: undefined,
  hideTopBar: false
};
exports.default = NavBoard;

/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NavItem = __webpack_require__(57);

var _NavItem2 = _interopRequireDefault(_NavItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function NavItem(_ref) {
  var Link = _ref.Link,
      item = _ref.item,
      navTitle = _ref.navTitle,
      onClick = _ref.onClick;

  if (item.external || !Link) {
    return _react2.default.createElement(
      'a',
      {
        key: item.name,
        className: _NavItem2.default['nav-board-link'],
        href: item.url,
        'data-ga-category': '\u5074\u9078\u55AE',
        'data-ga-action': navTitle,
        'data-ga-label': item.title,
        onClick: onClick
      },
      item.title
    );
  }

  return _react2.default.createElement(
    'span',
    { key: item.name, onClick: onClick },
    _react2.default.createElement(
      Link,
      { route: item.url },
      _react2.default.createElement(
        'a',
        {
          className: _NavItem2.default['nav-board-link'],
          'data-ga-category': '\u5074\u9078\u55AE',
          'data-ga-action': navTitle,
          'data-ga-label': item.title
        },
        item.title
      )
    )
  );
}

NavItem.propTypes = {
  Link: _propTypes2.default.func,
  item: _propTypes2.default.shape({
    name: _propTypes2.default.string.isRequired,
    title: _propTypes2.default.string.isRequired,
    url: _propTypes2.default.string.isRequired,
    external: _propTypes2.default.bool
  }).isRequired,
  navTitle: _propTypes2.default.string.isRequired,
  onClick: _propTypes2.default.func
};

NavItem.defaultProps = {
  Link: undefined,
  onClick: undefined
};

exports.default = NavItem;

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"nav-board-link":"_3Nksy"};

/***/ }),

/***/ 58:
/***/ (function(module, exports) {

module.exports = [{"name":"mweb","title":"新聞","items":[{"name":"headline","title":"即時頭條","url":"https://m.cnyes.com/news/cat/headline","external":true},{"name":"news24h","title":"主編精選","url":"https://m.cnyes.com/news/cat/news24h","external":true},{"name":"tw_stock","title":"台股","url":"https://m.cnyes.com/news/cat/tw_stock","external":true},{"name":"wd_stock","title":"國際股","url":"https://m.cnyes.com/news/cat/wd_stock","external":true},{"name":"forex","title":"外匯","url":"https://m.cnyes.com/news/cat/forex","external":true},{"name":"future","title":"商品期貨","url":"https://m.cnyes.com/news/cat/future","external":true},{"name":"tw_money","title":"理財","url":"https://m.cnyes.com/news/cat/tw_money","external":true},{"name":"cn_stock","title":"A股港股","url":"https://m.cnyes.com/news/cat/cn_stock","external":true},{"name":"cnyeshouse","title":"房產","url":"https://m.cnyes.com/news/cat/cnyeshouse","external":true},{"name":"celebrity_area","title":"鉅亨新視界","url":"https://m.cnyes.com/news/cat/celebrity_area","external":true},{"name":"popular","title":"人氣新聞","url":"https://m.cnyes.com/news/cat/popular","external":true},{"name":"topic","title":"專題報導","url":"https://topics.cnyes.com","external":true}]},{"name":"forex","title":"外匯","items":[{"name":"USD","title":"美元","url":"https://forex.cnyes.com/currency/USD/TWD","external":true},{"name":"JPY","title":"日幣","url":"https://forex.cnyes.com/currency/JPY/TWD","external":true},{"name":"EUR","title":"歐元","url":"https://forex.cnyes.com/currency/EUR/TWD","external":true},{"name":"KRW","title":"韓幣","url":"https://forex.cnyes.com/currency/KRW/TWD","external":true},{"name":"CNY","title":"人民幣","url":"https://forex.cnyes.com/currency/CNY/TWD","external":true},{"name":"HKD","title":"港幣","url":"https://forex.cnyes.com/currency/HKD/TWD","external":true},{"name":"ZAR","title":"南非幣","url":"https://forex.cnyes.com/currency/ZAR/TWD","external":true},{"name":"AUD","title":"澳幣","url":"https://forex.cnyes.com/currency/AUD/TWD","external":true}]},{"name":"crypto","title":"虛擬貨幣","items":[{"name":"BTC","title":"比特幣","url":"https://crypto.cnyes.com/BTC/24h","external":true},{"name":"ETH","title":"以太幣","url":"https://crypto.cnyes.com/ETH/24h","external":true},{"name":"XRP","title":"Ripple","url":"https://crypto.cnyes.com/XRP/24h","external":true},{"name":"BCH","title":"Bitcoin Cash","url":"https://crypto.cnyes.com/BCH/24h","external":true},{"name":"LTC","title":"Litcoin","url":"https://crypto.cnyes.com/LTC/24h","external":true},{"name":"EOS","title":"EOS","url":"https://crypto.cnyes.com/EOS/24h","external":true}]},{"name":"market","title":"市場","items":[{"name":"market_taiwan","title":"台股指數","url":"https://m.cnyes.com/market/#market_taiwan","external":true},{"name":"market_global","title":"國際指數","url":"https://m.cnyes.com/market/#market_global","external":true},{"name":"market_currency","title":"國際外匯","url":"https://m.cnyes.com/market/#market_currency","external":true},{"name":"index_tse","title":"台股","url":"https://m.cnyes.com/twstock/index_tse.aspx","external":true},{"name":"mystock","title":"自選股","url":"https://m.cnyes.com/person/mystock.aspx","external":true},{"name":"news/search","title":"個股查詢","url":"https://m.cnyes.com/news/search","external":true},{"name":"theme","title":"主題投資","url":"http://theme.cnyes.com","external":true},{"name":"fund","title":"基金","url":"https://fund.cnyes.com","external":true}]},{"name":"channels","title":"頻道","items":[{"name":"blog","title":"Blog","url":"http://m.blog.cnyes.com","external":true}]}]

/***/ }),

/***/ 59:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cat-board-nav":"_wwdaL","nav-board-top-bar":"_2EEsr","nav-board":"_3RjfV","nav-board-show":"_38glh","nav-board-logo":"_23siC","nav-board-btn-close":"_-kDn3"};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ })

/******/ });
});
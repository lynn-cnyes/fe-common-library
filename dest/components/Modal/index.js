(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react")) : factory(root["prop-types"], root["react"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 143);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 143:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(144);


/***/ }),

/***/ 144:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _bind = __webpack_require__(26);

var _bind2 = _interopRequireDefault(_bind);

var _preventScroll = __webpack_require__(145);

var _preventScroll2 = _interopRequireDefault(_preventScroll);

var _Modal = __webpack_require__(146);

var _Modal2 = _interopRequireDefault(_Modal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var cx = _bind2.default.bind(_Modal2.default);

var Modal = function (_React$Component) {
  _inherits(Modal, _React$Component);

  function Modal() {
    _classCallCheck(this, Modal);

    return _possibleConstructorReturn(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).apply(this, arguments));
  }

  _createClass(Modal, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      if (this.props.isOpen) _preventScroll2.default.on();else _preventScroll2.default.off();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      _preventScroll2.default.off();
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          isOpen = _props.isOpen,
          minHeight = _props.minHeight;


      return _react2.default.createElement(
        'div',
        { className: cx('modal-wrapper', isOpen ? 'visible' : 'invisible') },
        _react2.default.createElement(
          'div',
          { className: cx('modal'), style: { minHeight: minHeight + 'px' } },
          _react2.default.createElement(
            'div',
            { className: cx('anue-line') },
            _react2.default.createElement('div', { className: cx('blue') }),
            _react2.default.createElement('div', { className: cx('red') }),
            _react2.default.createElement('div', { className: cx('yellow') })
          ),
          this.props.children
        )
      );
    }
  }]);

  return Modal;
}(_react2.default.Component);

Modal.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.element, _propTypes2.default.arrayOf(_propTypes2.default.element)]),
  isOpen: _propTypes2.default.bool.isRequired,
  minHeight: _propTypes2.default.number
};
Modal.defaultProps = {
  children: null,
  minHeight: 0
};

Modal.Image = function (_ref) {
  var children = _ref.children;
  return _react2.default.createElement(
    'div',
    { className: cx('modal__image') },
    children
  );
};

Modal.Title = function (_ref2) {
  var children = _ref2.children;
  return _react2.default.createElement(
    'div',
    { className: cx('modal__title') },
    children
  );
};

Modal.Description = function (_ref3) {
  var children = _ref3.children;
  return _react2.default.createElement(
    'div',
    { className: cx('modal__description') },
    children
  );
};

Modal.MinorButton = function (_ref4) {
  var children = _ref4.children,
      onClick = _ref4.onClick,
      disabled = _ref4.disabled;
  return _react2.default.createElement(
    'button',
    { className: cx('modal__button--minor'), onClick: onClick, disabled: !!disabled },
    children
  );
};

Modal.MajorButton = function (_ref5) {
  var children = _ref5.children,
      onClick = _ref5.onClick,
      disabled = _ref5.disabled;
  return _react2.default.createElement(
    'button',
    { className: cx('modal__button--major'), onClick: onClick, disabled: !!disabled },
    children
  );
};

exports.default = Modal;

/***/ }),

/***/ 145:
/***/ (function(module, exports, __webpack_require__) {

(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["preventScroll"] = factory();
	else
		root["preventScroll"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var disabled = false;
	var currentPosition = void 0,
	    currentOverflow = void 0,
	    currentWidth = void 0;
	
	exports.default = {
	  on: function on() {
	    if (disabled) {
	      return;
	    }
	
	    disabled = true;
	
	    var htmlEl = document.querySelector('html');
	    var body = document.body;
	
	    // Determine the `scrollTop` to use. Some browsers require checking the
	    // `body`, others use `html`.
	    var bodyScrollTop = body.scrollTop;
	    var htmlScrollTop = htmlEl.scrollTop;
	    var scrollTop = bodyScrollTop ? bodyScrollTop : htmlScrollTop;
	
	    // Store the current value of the htmlEl's styles – we're about to override
	    // them.
	    currentPosition = htmlEl.style.position;
	    currentOverflow = htmlEl.style.overflowY;
	    currentWidth = htmlEl.style.width;
	
	    // Fixing the position of the `htmlEl` prevents the page from scrolling
	    // at all.
	    htmlEl.style.position = 'fixed';
	    // Setting `overflowY` to `scroll` ensures that any scrollbars that are
	    // around stick around. Otherwise, there would be a "jump" when the page
	    // becomes unscrollable as the bar would vanish.
	    htmlEl.style.overflowY = 'scroll';
	    // This makes sure that the page doesn't collapse (usually your CSS will
	    // prevent this, but it's best to be safe)
	    htmlEl.style.width = '100%';
	    // Scoot down the `htmlEl` to be in the same place that the user had
	    // scrolled to.
	    htmlEl.style.top = '-' + scrollTop + 'px';
	  },
	  off: function off() {
	    if (!disabled) {
	      return;
	    }
	
	    disabled = false;
	
	    var htmlEl = document.querySelector('html');
	    var body = document.body;
	
	    // Reset `htmlEl` to the original styles.
	    htmlEl.style.position = currentPosition;
	    htmlEl.style.overflowY = currentOverflow;
	    htmlEl.style.width = currentWidth;
	
	    // Retrieve our original scrollTop from the htmlEl's top
	    var scrollTop = -parseInt(htmlEl.style.top);
	    // Return us to the original scroll position. Once again, we set this on
	    // both the `body` and the `htmlEl` to be safe.
	    htmlEl.scrollTop = scrollTop;
	    body.scrollTop = scrollTop;
	  }
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=prevent-scroll.js.map

/***/ }),

/***/ 146:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"modal-wrapper":"_2w2O1","modal":"_9QpxU","anue-line":"_3kkt2","blue":"_3HTyz","red":"_3pe9W","yellow":"_UGg15","visible":"_3j2Gf","invisible":"_Bm67o","modal__image":"_35I3y","modal__title":"_2TAVh","modal__description":"_2zggk","modal__button--minor":"_VJkJ8","modal__button--major":"_3ezbH"};

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2016 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(this && this[arg] || arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(this, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(this && this[key] || key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
		window.classNames = classNames;
	}
}());


/***/ })

/******/ });
});
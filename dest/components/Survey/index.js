(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react")) : factory(root["prop-types"], root["react"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 136);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 136:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(60);


/***/ }),

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  setItem: function setItem(key, value) {
    try {
      window.localStorage.setItem(key, value);
    } catch (e) {
      // ignore
    }
  },
  getItem: function getItem(key) {
    try {
      return window.localStorage.getItem(key);
    } catch (e) {
      // ignore
    }

    return null;
  },
  removeItem: function removeItem(key) {
    try {
      window.localStorage.removeItem(key);
    } catch (e) {
      // ignore
    }
  },
  filter: function filter(filterFunction) {
    var localStorageLength = void 0;

    try {
      localStorageLength = window.localStorage.length;
    } catch (e) {
      // ignore

      return [];
    }

    var keys = [];

    for (var i = 0; i < localStorageLength; i += 1) {
      var key = window.localStorage.key(i);

      if (filterFunction(key)) keys.push(key);
    }

    return keys;
  }
};

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _getStyleName = __webpack_require__(4);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _localStorageWrapper = __webpack_require__(17);

var _localStorageWrapper2 = _interopRequireDefault(_localStorageWrapper);

var _Survey = __webpack_require__(61);

var _Survey2 = _interopRequireDefault(_Survey);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EXPIRED_AFTER = 6 * 30 * 24 * 60 * 60 * 1000; // 六個月

var Survey = function (_PureComponent) {
  _inherits(Survey, _PureComponent);

  function Survey(props) {
    _classCallCheck(this, Survey);

    var _this = _possibleConstructorReturn(this, (Survey.__proto__ || Object.getPrototypeOf(Survey)).call(this, props));

    _this.removeExpiredSurveyRecords = function () {
      var now = new Date();

      _localStorageWrapper2.default.filter(function (key) {
        return key.includes('surveyNewsId:') && now - _localStorageWrapper2.default.getItem(key) > EXPIRED_AFTER;
      }).forEach(_localStorageWrapper2.default.removeItem);
    };

    _this.handleClick = function () {
      _localStorageWrapper2.default.setItem('surveyNewsId:' + _this.props.newsId, new Date().getTime());

      _this.setState({ clicked: true });
    };

    _this.state = {
      clicked: false
    };

    _this.removeExpiredSurveyRecords();

    // 如果 localStorage 裡有包含該新聞 ID 的 key，代表六個內使用者回答過該新聞的調查，便不再顯示調查框
    if (_localStorageWrapper2.default.getItem('surveyNewsId:' + _this.props.newsId)) _this.surveyed = true;
    return _this;
  }

  _createClass(Survey, [{
    key: 'render',
    value: function render() {
      var clicked = this.state.clicked;


      if (this.surveyed) return null;

      if (clicked) {
        return _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey') },
          '\u611F\u8B1D\u60A8\u7684\u5BF6\u8CB4\u610F\u898B'
        );
      }

      var newsId = this.props.newsId;


      return _react2.default.createElement(
        'div',
        { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey') },
        _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey-question') },
          '\u9019\u7BC7\u65B0\u805E\u5C0D\u60A8\u5728\u6295\u8CC7\u4E0A\u662F\u5426\u6709\u5E6B\u52A9\uFF1F'
        ),
        _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey-options') },
          _react2.default.createElement(
            'button',
            {
              'data-ga-category': '\u5167\u5BB9\u662F\u5426\u6709\u52A9\u6295\u8CC7',
              'data-ga-action': '\u6709\u5E6B\u52A9',
              'data-ga-label': newsId,
              onClick: this.handleClick
            },
            '\u662F'
          ),
          _react2.default.createElement(
            'button',
            {
              'data-ga-category': '\u5167\u5BB9\u662F\u5426\u6709\u52A9\u6295\u8CC7',
              'data-ga-action': '\u6C92\u5E6B\u52A9',
              'data-ga-label': newsId,
              onClick: this.handleClick
            },
            '\u5426'
          )
        )
      );
    }
  }]);

  return Survey;
}(_react.PureComponent);

Survey.propTypes = {
  newsId: _propTypes2.default.number.isRequired
};
exports.default = Survey;

/***/ }),

/***/ 61:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cnyes-survey":"_1UwPA","cnyes-survey-question":"_35aIo","cnyes-survey-options":"_28KgD"};

/***/ })

/******/ });
});
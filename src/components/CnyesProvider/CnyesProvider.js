import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { authType, requestType } from '../../utils/propTypes';

class CnyesProvider extends PureComponent {
  static propTypes = {
    auth: authType.isRequired,
    request: requestType.isRequired,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  };

  static defaultProps = {
    children: undefined,
  };

  static childContextTypes = {
    auth: authType,
    request: requestType,
  };

  getChildContext() {
    return {
      auth: this.props.auth,
      request: this.props.request,
    };
  }

  render() {
    return this.props.children;
  }
}

export default CnyesProvider;

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import Footer from '../Footer';
import defaultNotes from './defaultFooter.md';

storiesOf('Footer', module)
  .addDecorator(story => (
    <div style={{ position: 'absolute', left: 0, top: 0, width: '100%', height: '100%' }}>
      <p style={{ padding: '80px 20px' }}>long content here...</p>
      <div style={{ position: 'absolute', left: 0, bottom: 0, width: '100%' }}>{story()}</div>
    </div>
  ))
  .add('default', withInfo()(withNotes(defaultNotes)(() => <Footer now={Date.now()} />)));

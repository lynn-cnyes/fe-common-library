import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import LoginPage from '../LoginPage';

const stories = storiesOf('FundsyesOldDriver/LoginPage', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'default',
  withNotes('')(
    withInfo()(() => {
      return <LoginPage />;
    })
  )
);

## Usage

```
import Header from 'fe-common-library/dest/components/Header';
import 'fe-common-library/dest/components/Header/style.css';

// ...

render() {
  return (
    <Header
      channel="首頁"
      navs={[...]}
      location={window.location}
    />;
  );
}
```
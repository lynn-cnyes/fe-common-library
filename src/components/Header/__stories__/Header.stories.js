/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import { withState } from '@dump247/storybook-state';
import * as knobs from '@storybook/addon-knobs/react';
import Link from './mockLink';
import Header from '../Header';
// import '../StaticHeader.scss';
import defaultNotes from './Header.md';
import * as fixedHeaderTypes from '../ConstantUI';
import * as simpleConfig from './config/simpleConfig';
import * as newsConfig from './config/newsConfig';

const stories = storiesOf('Header', module);

// stories.addDecorator((story, context) => withInfo()(story)(context));
stories.addDecorator(knobs.withKnobs);
stories.addDecorator(story => (
  <div>
    {story()}
    <div style={{ width: '100%', height: '1500px', padding: '80px 0' }}>long content here...</div>
  </div>
));

stories.add(
  'change channel state',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const channelOptions = simpleConfig.navs.map(o => o.title);

      return (
        <Header
          channel={knobs.select('channel', channelOptions, channelOptions[0])}
          displayChannelName={knobs.boolean('displayChannelName', true)}
          navs={simpleConfig.navs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
        />
      );
    })
  )
);

stories.add(
  'toggle fixed header',
  withNotes(defaultNotes)(
    withState({ fixedHeaderType: fixedHeaderTypes.FIXED_HEADER_FULL })(
      withInfo()(({ store }) => {
        return (
          <Header
            channel="新聞"
            navs={newsConfig.navs}
            catNavs={newsConfig.catNavs}
            location={window.location}
            fixedHeaderType={knobs.select('fixedHeaderType', fixedHeaderTypes, store.state.fixedHeaderType)}
            stickySubHeader={knobs.boolean('stickySubHeader', true)}
            stickySearchHeader={knobs.boolean('stickySearchHeader', true)}
            toggleFixedHeader={type => {
              store.set({ fixedHeaderType: type });
              action('toggleFixedHeader')(type);
            }}
          />
        );
      })
    )
  )
);

stories.add(
  'customize Link',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const useMockLink = knobs.boolean('use customized Link with action logger?', true);

      return (
        <Header
          channel="新聞"
          Link={useMockLink ? Link : undefined}
          navs={newsConfig.navs}
          catNavs={newsConfig.catNavs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
        />
      );
    })
  )
);

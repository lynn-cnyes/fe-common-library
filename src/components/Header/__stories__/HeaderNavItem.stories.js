/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import Header from '../Header';
// import '../StaticHeader.scss';
import defaultNotes from './Header.md';
import * as fixedHeaderTypes from '../ConstantUI';
import { navs } from './config/simpleConfig';

const stories = storiesOf('Header/Nav Item', module);

stories.addDecorator((story, context) => withInfo()(story)(context));
stories.addDecorator(knobs.withKnobs);
stories.addDecorator(story => (
  <div>
    {story()}
    <div style={{ width: '100%', height: '1500px', padding: '80px 0' }}>long content here...</div>
  </div>
));

stories.add(
  'default',
  withNotes(defaultNotes)(() => {
    return (
      <Header
        channel={knobs.text('channel', '首頁')}
        navs={knobs.object('navs', navs)}
        location={window.location}
        fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
      />
    );
  })
);

stories.add(
  'with leftList',
  withNotes(defaultNotes)(() => {
    const channel = knobs.select('channel', ['首頁', '台股'], '首頁');
    const customizedNavItem = knobs.object('台股', {
      title: '台股',
      url: 'http://www.cnyes.com/twstock/index.htm',
      catSlug: 'tw_stock',
      isHeadline: true,
      leftList: [
        { title: '台指期', url: 'http://www.cnyes.com/twfutures/index.htm' },
        { title: '選擇權', url: 'http://www.cnyes.com/twoption/' },
        { title: '權證', url: 'http://www.cnyes.com/warrant/tw/index.htm' },
        { title: '興櫃', url: 'http://www.cnyes.com/presh/index.htm' },
        { title: '未上市', url: 'http://www.cnyes.com/pre/index.htm' },
      ],
    });
    const displayNavs = [navs[0], customizedNavItem];

    return (
      <Header
        channel={channel}
        navs={displayNavs}
        location={window.location}
        fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
      />
    );
  })
);

stories.add(
  'with rightList',
  withNotes(defaultNotes)(() => {
    const channel = knobs.select('channel', ['首頁', '交易'], '首頁');
    const customizedNavItem = knobs.object('交易', {
      title: '交易',
      url: '',
      leftList: [
        { title: '基金交易', url: 'https://www.fundsyes.com/Index.aspx?utm_source=cnyes&utm_medium=channel_mainpage' },
      ],
      rightListTitle: '鉅亨基金交易平台',
      rightList: [
        { title: '交易登入', url: 'https://www.fundsyes.com/Login.aspx?utm_source=cnyes&utm_medium=channel_login' },
        {
          title: '免費開戶',
          url: 'https://www.fundsyes.com/launch/fund_index.htm?utm_source=cnyes&utm_medium=channel_newuser',
        },
        {
          title: '最新優惠',
          url: 'https://www.fundsyes.com/Guide/Expenses.aspx?utm_source=cnyes&utm_medium=channel_preferential',
        },
        {
          title: '投資雷達',
          url: 'https://www.fundsyes.com/Tool/SuggestionList.aspx?utm_source=cnyes&utm_medium=channel_radar',
        },
      ],
    });
    const displayNavs = [navs[0], customizedNavItem];

    return (
      <Header
        channel={channel}
        navs={displayNavs}
        location={window.location}
        fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
      />
    );
  })
);

export const navs = [
  {
    title: '首頁',
    url: 'https://www.cnyes.com',
  },
  {
    title: '新聞',
    url: 'https://news.cnyes.com/',
    catSlug: 'news',
    isHeadline: true,
    leftList: [
      { title: '頭條', url: 'https://news.cnyes.com/news/cat/headline' },
      { title: '總覽', url: 'https://news.cnyes.com/news/cat/all' },
      { title: '人氣', url: 'https://news.cnyes.com/trending' },
      { title: '虛擬貨幣', url: 'https://news.cnyes.com/search?group=1&q=虛擬貨幣' },
    ],
  },
];

export const catNavs = [
  {
    name: 'headline',
    url: '/news/cat/headline',
    title: '頭條',
  },
  {
    name: 'all',
    url: '/news/cat/all',
    title: '總覽',
  },
  {
    name: 'trending',
    url: '/trending',
    title: '人氣',
  },
  {
    name: 'searchCrypto',
    url: '/search?group=1&q=虛擬貨幣',
    title: '虛擬貨幣',
    isNew: true,
  },
];

/* eslint-disable no-multi-spaces */

const navs = [
  {
    title: '關於我們',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_about.html',
  },
  {
    title: '廣告服務',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_AD01.html',
  },
  {
    title: '金融資訊元件',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_pas01.html',
  },
  {
    title: '聯絡我們',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_ctcUsTpe.html',
  },
  {
    title: '徵才',
    url: 'https://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=5e6042253446402330683b1d1d1d1d5f2443a363189j01',
  },
  {
    title: '網站地圖',
    url: 'http://www.cnyes.com/cnyes_about/site_map.html',
  },
  {
    title: '法律聲明',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_sos01.html',
  },
];

export default navs;

const catNavs = [
  {
    name: 'BankService',
    url: 'http://www.cnyes.com/money/BankService.aspx?ga=nav',
    title: '銀行服務',
    external: true,
    subItems: [
      {
        name: 'PublicRate',
        url: 'http://www.cnyes.com/money/PublicRate.htm?ga=nav',
        title: '牌告利率',
        external: true,
      },
      {
        name: 'ValutaDepositRate',
        url: 'http://www.cnyes.com/money/ValutaDepositRate.htm?ga=nav',
        title: '外幣存款利率',
        external: true,
      },
      {
        name: 'DepositRate',
        url: 'http://www.cnyes.com/money/DepositRate.htm?ga=nav',
        title: '存款利率',
        external: true,
      },
      {
        name: 'UsuryRate',
        url: 'http://www.cnyes.com/money/UsuryRate.htm?ga=nav',
        title: '放款利率',
        external: true,
      },
      {
        name: 'BankMoneyExchange',
        url: 'http://www.cnyes.com/money/BankMoneyExchange.htm?ga=nav',
        title: '銀行換匯',
        external: true,
      },
      {
        name: 'BankMoneyExchangeCompareTable',
        url: 'http://www.cnyes.com/money/BankMoneyExchangeCompareTable.htm?ga=nav',
        title: '銀行換匯比較表',
        external: true,
      },
    ],
  },
  {
    name: 'StockQ',
    url: 'http://www.cnyes.com/money/BankCalculation.aspx?ga=nav',
    title: '試算工具',
    external: true,
  },
  {
    name: 'tw_money',
    url: 'https://news.cnyes.com/news/cat/tw_money',
    title: '理財新聞',
    external: true,
  },
  {
    name: 'fixedincome_index',
    url: 'http://www.cnyes.com/fixedincome/Index.htm?ga=nav',
    title: '固定收益',
    external: true,
    subItems: [
      {
        name: 'securitiesYield_currentYield',
        url: 'http://www.cnyes.com/fixedincome/Page/securitiesYield_currentYield.aspx?ga=nav',
        title: '股票殖利率',
        external: true,
      },
      {
        name: 'fixedFund',
        url: 'http://www.cnyes.com/fixedincome/Page/fixedFund.htm?ga=nav',
        title: '固定收益基金',
        external: true,
      },
      {
        name: 'Etf_Screener',
        url: 'http://www.cnyes.com/fixedincome/Page/Etf_Screener.htm?ga=nav',
        title: '固定收益ETF',
        external: true,
      },
      {
        name: 'bankDeposit',
        url: 'http://www.cnyes.com/fixedincome/Page/bankDeposit.aspx?ga=nav',
        title: '銀行存款',
        external: true,
      },
      {
        name: 'Bond',
        url: 'http://www.cnyes.com/fixedincome/Page/Bond.htm?ga=nav',
        title: '債券',
        external: true,
      },
      {
        name: 'Resl',
        url: 'http://www.cnyes.com/fixedincome/Page/Resl.htm?ga=nav',
        title: '不動產證券化',
        external: true,
      },
      {
        name: 'astock',
        url: 'http://www.cnyes.com/astock/',
        title: '人民幣',
        external: true,
      },
      {
        name: 'tax',
        url: 'http://www.cnyes.com/fixedincome/Page/tax.htm?ga=nav',
        title: '相關稅務',
        external: true,
      },
      {
        name: 'AllCompare',
        url: 'http://www.cnyes.com/fixedincome/Page/AllCompare.htm?ga=nav',
        title: '商品比較',
        external: true,
      },
    ],
  },
];

export default catNavs;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import styles from './List.scss';
import { rwdWidthMode } from './constants';

const cx = classnames.bind(styles);

class List extends Component {
  render() {
    const { widthMode, children } = this.props;
    const className = widthMode === rwdWidthMode.short ? 'list-width-short' : 'list-width-wide';

    return (
      <div className={cx('list', className)}>
        {React.Children.map(children, child =>
          React.cloneElement(child, {
            widthMode,
          })
        )}
      </div>
    );
  }
}

List.propTypes = {
  widthMode: PropTypes.oneOf(Object.values(rwdWidthMode)),
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};

List.defaultProps = {
  widthMode: rwdWidthMode.short,
};

export default List;

```
import { List, OrderListItem } from 'fe-common-library/dest/components/List';
import 'fe-common-library/dest/components/List/style.css';

...

render() {
  return (
    <List>
      {
        items.map(({ id, order, title, time }) => (
          <OrderListItem
            key={id}
            order={order}
            title={title}
            time={time}
            now={now}
            handleClick={this.handleItemClick}
          />
        ))
      }
    </List>
  );
}
```
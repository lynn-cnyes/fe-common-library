/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import { action } from '@storybook/addon-actions';
import OrderListItem from '../OrderListItem';
import List from '../List';
import defaultNotes from './List.md';

const stories = storiesOf('List', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'OrderList',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const now = Date.now();
      const items = [
        { id: 'a', title: '台股期權盤後－三角收斂依舊 台股打底作撐', time: now / 1000 },
        { id: 'b', title: '唱衰美元聲量大 但和歐元相比 美元明年仍續強', time: now / 1000 - 60 * 60 * 24 },
        {
          id: 'c',
          title: '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司明年3月恐下市',
          time: now / 1000 - 60 * 60 * 24 * 30,
        },
      ];

      return (
        <div style={{ padding: '20px', background: '#F1F1F1', height: '100%' }}>
          <h3 style={{ fontSize: '20px', margin: '10px' }}>widthMode: SHORT</h3>
          <List>
            {items.map(({ id, title, time }, index) => (
              <OrderListItem
                key={id}
                order={index + 1}
                title={title}
                time={time}
                now={now}
                handleClick={action(title)}
              />
            ))}
          </List>
          <h3 style={{ fontSize: '20px', margin: '10px' }}>widthMode: WIDE</h3>
          <List widthMode="WIDE">
            {items.map(({ id, title, time }, index) => (
              <OrderListItem
                key={id}
                order={index + 1}
                title={title}
                time={time}
                now={now}
                handleClick={action(title)}
              />
            ))}
          </List>
        </div>
      );
    })
  )
);

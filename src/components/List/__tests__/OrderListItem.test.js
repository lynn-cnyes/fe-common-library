import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import OrderListItem from '../OrderListItem';
import SmartTime from '../../SmartTime/SmartTime';

describe('<List />', () => {
  let makeSubject;
  let subject;

  beforeEach(() => {
    jest.resetModules();
    makeSubject = ({ order, title, widthMode = 'SHORT', time }) =>
      mount(<OrderListItem order={order} title={title} widthMode={widthMode} time={time} />);
  });

  it('should be same as snapshot', () => {
    subject = makeSubject({
      order: 1,
      title: '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司明年3月恐下市',
    });
    expect(toJson(subject)).toMatchSnapshot();
  });

  it('should adjust title base on widthMode', () => {
    const title = '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司明年3月恐下市';
    const shortTitle = '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司 ...';

    subject = makeSubject({
      order: 1,
      title,
    });
    expect(subject.find('h3').text()).toEqual(shortTitle);

    subject.setProps({ widthMode: 'WIDE' });
    expect(subject.find('h3').text()).toEqual(title);
  });

  it('should show SmartTime when add time prop', () => {
    subject = makeSubject({
      order: 1,
      title: '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司明年3月恐下市',
    });
    expect(subject.find(SmartTime).length).toEqual(0);

    subject.setProps({ time: Date.now() });
    expect(subject.find(SmartTime).length).toEqual(1);
  });
});

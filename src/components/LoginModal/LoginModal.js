import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import SimpleModal from '../../components/SimpleModal/SimpleModal';
import styles from './LoginModal.scss';
import { authType } from '../../utils/propTypes';

class LoginModal extends PureComponent {
  static propTypes = {
    visible: PropTypes.bool.isRequired,
    setVisible: PropTypes.func.isRequired,
    callLoadingModal: PropTypes.func.isRequired,
  };

  static contextTypes = {
    auth: authType,
  };

  closeHandler = () => {
    this.props.setVisible(false);
  };

  loginFactory = (loginMethod, loginMethodName) => () => {
    this.props.setVisible(false);
    this.props.callLoadingModal(true);

    const { auth } = this.context;

    // prettier-ignore
    auth[loginMethod]()
      .then(undefined, e => {
        console.error('login error', e);
        alert(`${loginMethodName} 登入時發生問題，請稍候再試。`);
      })
      .then(() => this.props.callLoadingModal(false));
  };

  loginFB = this.loginFactory('loginFB', 'Facebook');
  loginGoogle = this.loginFactory('loginGoogle', 'Google');

  render() {
    const { visible } = this.props;

    return (
      <SimpleModal enableModal={visible} closeHandler={this.closeHandler}>
        <div className={styles['login-modal-wrapper']}>
          <h2>登入</h2>
          <button className={styles['login-button-facebook']} onClick={this.loginFB} />
          <button className={styles['login-button-google']} onClick={this.loginGoogle} />
          <div className={styles.footer}>
            <p>
              註冊前請詳閱{' '}
              <a href="/agreement" target="_blank" rel="noopener noreferrer" className={styles.agreement}>
                會員服務條款
              </a>
            </p>
            <p>如您登入會員，即視為您已暸解並同意此條款。</p>
          </div>
        </div>
      </SimpleModal>
    );
  }
}

export default LoginModal;

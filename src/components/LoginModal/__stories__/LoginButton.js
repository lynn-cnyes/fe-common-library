import React, { Component } from 'react';
import propTypes from 'prop-types';
import styles from './LoginModal.stories.scss';
import { authType, userProfileType } from '../../../utils/propTypes';

class LoginButton extends Component {
  static propTypes = {
    onClick: propTypes.func.isRequired,
    isLogin: propTypes.bool,
    authIsFetching: propTypes.bool,
    userProfile: userProfileType,
  };

  static defaultProps = {
    isLogin: false,
    authIsFetching: false,
    userProfile: null,
  };

  static contextTypes = {
    auth: authType,
  };

  render() {
    if (this.props.isLogin) {
      return (
        <div>
          <div>{JSON.stringify(this.props.userProfile)}</div>
          <button className={styles['login-button']} onClick={this.context.auth.logout}>
            登出
          </button>
        </div>
      );
    }

    if (this.props.authIsFetching) {
      return <button className={styles['login-button']}>Loading...</button>;
    }

    return (
      <button className={styles['login-button']} onClick={this.props.onClick}>
        登入
      </button>
    );
  }
}

export default LoginButton;

/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback,prefer-promise-reject-errors */

import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import MobileMenu from '../MobileMenu';

describe('component <MobileMenu />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testParams = {
      showCatBoard: jest.fn(),
      hideTopBar: false,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testParams,
        ...params,
      };

      return mount(<MobileMenu {...props} />);
    };
  });

  it('should be render nice', () => {
    const subject = makeSubject();

    expect(subject.find('.index-header-logo')).toExist();
    expect(subject.find('.index-header-menu')).toExist();
    expect(subject.find('.header-channel-label')).not.toExist();
    expect(toJson(subject)).toMatchSnapshot();
  });

  describe('props.channelName', () => {
    it('should render channelName if channelName is defined', () => {
      const subject = makeSubject({
        channelName: '外匯',
      });

      expect(subject.find('.header-channel-label')).toExist();
    });

    it('should not render channelName if channelName is illegal', () => {
      const subject = makeSubject({
        channelName: '',
      });

      expect(subject.find('.header-channel-label')).not.toExist();
    });
  });

  describe('props.hideTopBar', () => {
    it('should have className .index-header-top-bar', () => {
      const subject = makeSubject();

      expect(subject.find('.index-header-top-bar').length).toBe(1);
    });

    it('should not have className .index-header-top-bar when hideTopBar is true', () => {
      const subject = makeSubject({ hideTopBar: true });

      expect(subject.find('.index-header-top-bar').length).toBe(0);
    });
  });
});

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import NavItem from './NavItem';
import navConfig from './navConfig.json';
import styles from './NavBoard.scss';
import getStyleName from '../../../utils/getStyleName';

export default class NavBoard extends PureComponent {
  static propTypes = {
    isCatBoardOpen: PropTypes.bool,
    hideCatBoard: PropTypes.func,
    hideTopBar: PropTypes.bool,
  };

  static defaultProps = {
    isCatBoardOpen: false,
    hideCatBoard: undefined,
    hideTopBar: false,
  };

  render() {
    const { hideTopBar, isCatBoardOpen, hideCatBoard } = this.props;
    const catBoardClassName = isCatBoardOpen ? styles['nav-board-show'] : styles['nav-board'];

    return (
      <aside
        className={cx(catBoardClassName, 'theme-gradient', {
          [getStyleName(styles, 'nav-board-top-bar')]: !hideTopBar,
        })}
      >
        <header>
          <span className={styles['nav-board-logo']}>鉅亨網</span>
          <button className={styles['nav-board-btn-close']} onClick={hideCatBoard} />
        </header>
        <main>
          {navConfig.map(nav => [
            <h4 key={`${nav.name}-h4`} className="nav-board-subtitle theme-nav-board-subtitle">
              {nav.title}
            </h4>,
            <nav key={`${nav.name}-nav`} className={styles['cat-board-nav']}>
              {nav.items && nav.items.map(item => <NavItem key={item.name} navTitle={nav.title} item={item} />)}
            </nav>,
          ])}
        </main>
      </aside>
    );
  }
}

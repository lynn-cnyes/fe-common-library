import React from 'react';
import PropTypes from 'prop-types';

import styles from './NavItem.scss';

function NavItem({ Link, item, navTitle, onClick }) {
  if (item.external || !Link) {
    return (
      <a
        key={item.name}
        className={styles['nav-board-link']}
        href={item.url}
        data-ga-category="側選單"
        data-ga-action={navTitle}
        data-ga-label={item.title}
        onClick={onClick}
      >
        {item.title}
      </a>
    );
  }

  return (
    <span key={item.name} onClick={onClick}>
      <Link route={item.url}>
        <a
          className={styles['nav-board-link']}
          data-ga-category="側選單"
          data-ga-action={navTitle}
          data-ga-label={item.title}
        >
          {item.title}
        </a>
      </Link>
    </span>
  );
}

NavItem.propTypes = {
  Link: PropTypes.func,
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    external: PropTypes.bool,
  }).isRequired,
  navTitle: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

NavItem.defaultProps = {
  Link: undefined,
  onClick: undefined,
};

export default NavItem;

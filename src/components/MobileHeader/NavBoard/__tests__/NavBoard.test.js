import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import NavBoard from '../NavBoard';
import navConfig from '../navConfig.json';

describe('<NavBoard />', () => {
  const makeSubject = (_props = {}) => {
    const params = {
      isCatBoardOpen: false,
      hideCatBoard: jest.fn(),
      hideTopBar: false,
    };

    const props = {
      ...params,
      ..._props,
    };

    return mount(<NavBoard {...props} />);
  };

  beforeEach(() => {
    jest.resetModules();
  });

  it('should render nice', () => {
    const subject = makeSubject();

    expect(subject.find('h4').length).toEqual(navConfig.length);
    expect(toJson(subject)).toMatchSnapshot();
  });

  describe('props.isCatBoardOpen', () => {
    it('should not use nav-board-show class at init, if isCatBoardOpen is false', () => {
      const subject = makeSubject();

      expect(subject.props().isCatBoardOpen).toEqual(false);
      expect(subject.find('aside')).not.toHaveClassName('nav-board-show');
      expect(subject.find('aside')).toHaveClassName('nav-board');
    });

    it('should render nice at init, if isCatBoardOpen is true', () => {
      const subject = makeSubject({
        isCatBoardOpen: true,
      });

      expect(subject.props().isCatBoardOpen).toEqual(true);
      expect(subject.find('aside')).toHaveClassName('nav-board-show');
      expect(subject.find('aside')).not.toHaveClassName('nav-board');
    });
  });

  it('should click close button work', () => {
    const mockHideCatBoard = jest.fn();
    const subject = makeSubject({
      hideCatBoard: mockHideCatBoard,
    });

    expect(mockHideCatBoard).not.toHaveBeenCalled();
    subject.find('.nav-board-btn-close').simulate('click');
    expect(mockHideCatBoard).toHaveBeenCalledTimes(1);
  });

  describe('props.hideTopBar', () => {
    it('should have className .nav-board-top-bar when hideTopBar is false as default value', () => {
      const subject = makeSubject();

      expect(subject.props().hideTopBar).toBeFalsy();
      expect(subject.find('.nav-board-top-bar').length).toBe(1);
    });

    it('should not have className .nav-board-top-bar when hideTopBar is true', () => {
      const subject = makeSubject({ hideTopBar: true });

      expect(subject.find('.nav-board-top-bar').length).toBe(0);
    });
  });
});

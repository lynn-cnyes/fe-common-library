/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback,prefer-promise-reject-errors */

import React from 'react';
import { mount } from 'enzyme';
import NavItem from '../NavItem';
import mockData from '../navConfig.json';

describe('component <NavItem />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testParams = {
      onClick: jest.fn(),
      item: mockData[0].items[0],
      navTitle: mockData[0].title,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testParams,
        ...params,
      };

      return mount(<NavItem {...props} />);
    };
  });

  it('should be nice, if external is true', () => {
    const subject = makeSubject();
    const item = subject.props().item;

    expect(item.external).toEqual(true);
    expect(subject.find('Link')).not.toExist();
    expect(subject.find('a')).toHaveText(item.title);
  });

  it('should be nice, if external is true even props.Link is defined', () => {
    const subject = makeSubject({
      Link: jest.fn(() => <div className="link" />),
    });
    const item = subject.props().item;

    expect(subject.props().Link).toBeDefined();
    expect(item.external).toEqual(true);
    expect(subject.find('.link')).not.toExist();
  });

  it('should be nice, if external is false but props.Link is undefined', () => {
    const subject = makeSubject({
      item: {
        ...mockData[0].items[0],
        external: false,
      },
    });
    const item = subject.props().item;

    expect(subject.props().Link).not.toBeDefined();
    expect(item.external).toEqual(false);
    expect(subject.find('.link')).not.toExist();
  });

  it('should be nice, if external is false but props.Link is defined', () => {
    const subject = makeSubject({
      Link: jest.fn(() => <div className="link" />),
      item: {
        ...mockData[0].items[0],
        external: false,
      },
    });
    const item = subject.props().item;

    expect(subject.props().Link).toBeDefined();
    expect(item.external).toEqual(false);
    expect(subject.find('.link')).toExist();
  });
});

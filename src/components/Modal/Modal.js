import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import preventScroll from 'prevent-scroll';
import styles from './Modal.scss';

const cx = classnames.bind(styles);

class Modal extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
    isOpen: PropTypes.bool.isRequired,
    minHeight: PropTypes.number,
  };

  static defaultProps = {
    children: null,
    minHeight: 0,
  };

  static Image = ({ children }) => <div className={cx('modal__image')}>{children}</div>;

  static Title = ({ children }) => <div className={cx('modal__title')}>{children}</div>;

  static Description = ({ children }) => <div className={cx('modal__description')}>{children}</div>;

  static MinorButton = ({ children, onClick, disabled }) => (
    <button className={cx('modal__button--minor')} onClick={onClick} disabled={!!disabled}>
      {children}
    </button>
  );

  static MajorButton = ({ children, onClick, disabled }) => (
    <button className={cx('modal__button--major')} onClick={onClick} disabled={!!disabled}>
      {children}
    </button>
  );

  componentDidUpdate() {
    if (this.props.isOpen) preventScroll.on();
    else preventScroll.off();
  }

  componentWillUnmount() {
    preventScroll.off();
  }

  render() {
    const { isOpen, minHeight } = this.props;

    return (
      <div className={cx('modal-wrapper', isOpen ? 'visible' : 'invisible')}>
        <div className={cx('modal')} style={{ minHeight: `${minHeight}px` }}>
          <div className={cx('anue-line')}>
            <div className={cx('blue')} />
            <div className={cx('red')} />
            <div className={cx('yellow')} />
          </div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Modal;

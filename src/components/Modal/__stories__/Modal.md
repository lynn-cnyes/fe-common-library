## Usage

```
import Modal from 'fe-common-library/dest/components/Modal/Modal';
import 'fe-common-library/dest/components/Modal/style.css';
import image from './image.svg';

// ...

render() {
  return (
    <Modal isOpen={isOpen}>
      <Modal.Image>
        <img src={image} width="100" height="59" alt="提領確認" />
      </Modal.Image>
      <Modal.Title>提領確認</Modal.Title>
      <Modal.Description>確認後將無法取消</Modal.Description>
      <Modal.MajorButton onClick={this.handleCancel}>取消</Modal.MajorButton>
      <Modal.MajorButton onClick={this.handleConfirm}>確認</Modal.MajorButton>
    </Modal>
  );
}
```
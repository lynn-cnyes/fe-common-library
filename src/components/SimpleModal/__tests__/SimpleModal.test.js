/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback,import/no-dynamic-require */

import React from 'react';
import { mount } from 'enzyme';

describe('SimpleModal', function() {
  let SimpleModal;

  beforeEach(() => {
    jest.resetModules();
    SimpleModal = require('../SimpleModal').default;

    this.params = {
      overlayStyle: {},
      containerStyle: {},
      closeHandleStyle: {},
      enableModal: true,
      enableClose: true,
      closeHandler: jest.fn(),
    };

    this.makeSubject = () => {
      const { overlayStyle, containerStyle, closeHandleStyle, enableModal, enableClose, closeHandler } = this.params;

      return mount(
        <SimpleModal
          overlayStyle={overlayStyle}
          containerStyle={containerStyle}
          closeHandleStyle={closeHandleStyle}
          enableModal={enableModal}
          enableClose={enableClose}
          closeHandler={closeHandler}
        >
          <div>some thing</div>
        </SimpleModal>
      );
    };
  });

  it('should be same as snapshot', () => {
    const subject = this.makeSubject();
    const tree = subject.html();

    expect(tree).toMatchSnapshot();
  });

  it('should be null if the params.enableModal is false', () => {
    this.params.enableModal = false;
    const subject = this.makeSubject();
    const tree = subject.html();

    expect(tree).toBeNull();
  });

  it('should call closeHandler function after calling the `handleClose` function', () => {
    const subject = this.makeSubject();

    subject.instance().handleClose({ preventDefault: jest.fn() });

    expect(this.params.closeHandler).toBeCalled();
  });
});

```
import SmartTime from 'fe-common-library/dest/components/SmartTime/SmartTime'

...

render() {
  return (
    <SmartTime second={second} nowMs={nowMs} format={format} />
  );
}
```
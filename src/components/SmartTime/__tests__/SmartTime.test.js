/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import SmartTime from '../SmartTime';

const SECONDS_IN_A_DAY = 86400;

describe('<SmartTime />', () => {
  let makeSubject;
  let subject;

  beforeEach(() => {
    jest.resetModules();
    makeSubject = ({ nowMS, second, format }) => mount(<SmartTime nowMs={nowMS} second={second} format={format} />);
  });

  const nowMS = 1542694016024; // Tue Nov 20 2018 14:06:56 GMT+0800

  describe('when second is now', () => {
    beforeEach(() => {
      const second = nowMS / 1000;

      subject = makeSubject({
        nowMS,
        second,
      });
    });

    it('should be same as snapshot', () => {
      expect(toJson(subject)).toMatchSnapshot();
    });

    it('should display 14:06', () => {
      expect(subject.text()).toEqual('14:06');
    });

    it('should has dateTime property', () => {
      expect(subject.html()).toContain('datetime="2018-11-20T14:06:56+08:00"');
    });
  });

  describe('when second is 1 days before', () => {
    beforeEach(() => {
      const second = nowMS / 1000 - 1 * SECONDS_IN_A_DAY;

      subject = makeSubject({
        nowMS,
        second,
      });
    });

    it('should be same as snapshot', () => {
      expect(toJson(subject)).toMatchSnapshot();
    });

    it('should display 11/19', () => {
      expect(subject.text()).toEqual('11/19');
    });

    it('should has dateTime property', () => {
      expect(subject.html()).toContain('datetime="2018-11-19T14:06:56+08:00"');
    });
  });

  describe('when second is 1 year before', () => {
    beforeEach(() => {
      const second = nowMS / 1000 - 365 * SECONDS_IN_A_DAY;

      subject = makeSubject({
        nowMS,
        second,
      });
    });

    it('should be same as snapshot', () => {
      expect(toJson(subject)).toMatchSnapshot();
    });

    it('should display 2017/11/20', () => {
      expect(subject.text()).toEqual('2017/11/20');
    });

    it('should has dateTime property', () => {
      expect(subject.html()).toContain('datetime="2017-11-20T14:06:56+08:00"');
    });
  });

  describe('custom format', () => {
    it('YYYY/MM/DD', () => {
      const second = nowMS / 1000;
      const format = 'YYYY/MM/DD';

      subject = makeSubject({
        second,
        format,
      });
      expect(subject.text()).toEqual('2018/11/20');
    });

    it('YYYY/MM/DD HH:mm', () => {
      const second = nowMS / 1000;
      const format = 'YYYY/MM/DD HH:mm';

      subject = makeSubject({
        second,
        format,
      });
      expect(subject.text()).toEqual('2018/11/20 14:06');
    });
  });
});

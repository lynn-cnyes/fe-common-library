import { fillZero, timeDisplayFactory, shortISOString, timeFormat } from '../timeHelper';

describe('timeHelper', () => {
  const now = 1542694016024; // Tue Nov 20 2018 14:06:56 GMT+0800

  beforeEach(() => {
    jest.resetModules();
  });

  it('fillZero', () => {
    expect(fillZero(3)).toEqual('03');
    expect(fillZero(30)).toEqual('30');
    expect(fillZero(0)).toEqual('00');
  });

  describe('timeDisplayFactory', () => {
    it('today', () => {
      expect(timeDisplayFactory(now, now)).toEqual('14:06');
    });

    it('last month', () => {
      expect(timeDisplayFactory(now - 60 * 60 * 24 * 30 * 1000, now)).toEqual('10/21');
    });

    it('last year', () => {
      expect(timeDisplayFactory(now - 60 * 60 * 24 * 365 * 1000, now)).toEqual('2017/11/20');
    });
  });

  describe('shortISOString', () => {
    let ms;
    const makeSubject = () => shortISOString(ms);

    [[1473921822277, '2016-09-15T14:43:42+08:00']].forEach(([input, expected]) => {
      describe(`when input is '${input}'`, () => {
        it('should same as expected', () => {
          ms = input;
          expect(makeSubject()).toEqual(expected);
        });
      });
    });

    [[''], [undefined]].forEach(([input]) => {
      describe(`when input is '${input}'`, () => {
        it('should return empty', () => {
          ms = input;
          expect(makeSubject()).toEqual('');
        });
      });
    });
  });

  describe('timeFormat', () => {
    it('default', () => {
      expect(timeFormat(now)).toEqual('2018/11/20');
    });

    it('YYYY/MM/DD', () => {
      expect(timeFormat(now, 'YYYY/MM/DD')).toEqual('2018/11/20');
    });

    it('YYYY/MM/DD HH:mm', () => {
      expect(timeFormat(now, 'YYYY/MM/DD HH:mm')).toEqual('2018/11/20 14:06');
    });
  });
});

import dayjs from 'dayjs';

export const fillZero = n => (n < 10 ? `0${n}` : `${n}`);

export const timeDisplayFactory = (timestampInMs, now) => {
  if (!timestampInMs) {
    return '';
  }

  const d = new Date(timestampInMs);
  const nowDate = new Date(now);
  const todayStartInTimestamp = nowDate.setHours(0, 0, 0, 0);
  const todayEndInTimestamp = nowDate.setHours(23, 59, 59, 999);

  // same day
  if (timestampInMs >= todayStartInTimestamp && timestampInMs <= todayEndInTimestamp) {
    return `${fillZero(d.getHours())}:${fillZero(d.getMinutes())}`;
  }

  // same year
  if (nowDate.getFullYear() === d.getFullYear()) {
    return `${fillZero(d.getMonth() + 1)}/${fillZero(d.getDate())}`;
  }

  // else
  return `${d.getFullYear()}/${fillZero(d.getMonth() + 1)}/${fillZero(d.getDate())}`;
};

/**
 * remove decimal fraction of a second
 */
export const shortISOString = ms => {
  if (!ms) {
    return '';
  }

  return dayjs(ms).format();
};

export const timeFormat = (ms, format = 'YYYY/MM/DD') => {
  return dayjs(ms).format(format);
};

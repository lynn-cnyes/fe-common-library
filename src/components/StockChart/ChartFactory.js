import idx from 'idx';
import ReactHighstock from 'react-highcharts/ReactHighstock';

const Highcharts = ReactHighstock.Highcharts;
const CHART_DEFAULT_HEIGHT = 144; // https://api.highcharts.com/highcharts/chart.height
const CHART_DEFAULT_WIDTH = 200; // https://api.highcharts.com/highcharts/chart.width
const CHART_DEFAULT_LINECOLOR = '#3d9ad9'; // https://api.highcharts.com/highcharts/series.spline.color
const CHART_CLOSELINE_COLOR = 'red';

export const InitialOptions = {
  global: {
    timezoneOffset: -8 * 60,
  },
  lang: {
    loading: '讀取中...',
    thousandsSep: ',',
  },
  credits: {
    enabled: false,
  },
};

export const defaultConfig = {
  credits: {
    enabled: false,
  },
  scrollbar: {
    enabled: false,
  },
  navigator: {
    enabled: false,
  },
  rangeSelector: {
    enabled: false,
  },
  legend: {
    enabled: false,
  },
};

export const ChartDataFactory = (response = null) => {
  const c = idx(response, _ => _.data.c);
  const t = idx(response, _ => _.data.t);

  if (c && t) {
    const result = [];
    let index = t.length - 1;

    while (index >= 0) {
      result.push([t[index] * 1000, c[index]]);
      index -= 1;
    }

    return result;
  }

  return null;
};

export const ChartConfigFactory = (detail = null) => {
  if (!detail) {
    return null;
  }

  const { chartData, configType, disableAnimation, lastClosePrice, customStyle } = detail;
  const { chartHeight, chartWidth, lineColor, closeLineColor } = customStyle;

  const customConfig = {
    '24h': {
      xDateTimeLabelFormats: {
        millisecond: '%H:%M',
        second: '%H:%M',
        minute: '%H:%M',
        hour: '%H:%M',
        day: '%H:%M',
        week: '%H:%M',
        month: '%H:%M',
        year: '%H:%M',
      },
      tooltipDateFormat: '%H:%M',
    },
    '3m': {
      xDateTimeLabelFormats: {
        millisecond: '%m-%d',
        second: '%m-%d',
        minute: '%m-%d',
        hour: '%m-%d',
        day: '%m-%d',
        week: '%m-%d',
        month: '%m-%d',
        year: '%m-%d',
      },
      tooltipDateFormat: '%Y-%m-%d',
    },
    '1y': {
      xDateTimeLabelFormats: {
        millisecond: '%m-%d',
        second: '%m-%d',
        minute: '%m-%d',
        hour: '%m-%d',
        day: '%m-%d',
        week: '%m-%d',
        month: '%m-%d',
        year: '%m-%d',
      },
      tooltipDateFormat: '%Y-%m-%d',
    },
    '3y': {
      xDateTimeLabelFormats: {
        millisecond: '%Y-%m',
        second: '%Y-%m',
        minute: '%Y-%m',
        hour: '%Y-%m',
        day: '%Y-%m',
        week: '%Y-%m',
        month: '%Y-%m',
        year: '%Y-%m',
      },
      tooltipDateFormat: '%Y-%m-%d',
    },
  };
  const series = [
    {
      data: chartData,
      dataGrouping: {
        enabled: false,
      },
      color: lineColor || CHART_DEFAULT_LINECOLOR,
    },
  ];

  if (lastClosePrice !== null) {
    series.push({
      data: chartData
        .filter((data, index, arr) => index === 0 || index === arr.length - 1)
        .map(data => [data[0], lastClosePrice]),
      color: closeLineColor || CHART_CLOSELINE_COLOR,
    });
  }

  return {
    ...defaultConfig,
    chart: {
      type: 'spline',
      reflow: false,
      height: chartHeight || CHART_DEFAULT_HEIGHT,
      width: chartWidth || CHART_DEFAULT_WIDTH,
    },
    series,
    tooltip: {
      shape: 'square',
      split: false,
      useHTML: true,
      borderWidth: 0,
      backgroundColor: null,
      formatter: function formatter() {
        const point = this.point;
        const date = point.x;
        const price = point.y;
        const dateFormat = customConfig[configType].tooltipDateFormat;

        return `
          <div class="coin-chart-tooltip">
            <span>${price}</span><br />
            <span>[${Highcharts.dateFormat(dateFormat, new Date(date))}]</span>
          </div>
        `;
      },
    },
    xAxis: {
      crosshair: {
        color: 'rgba(0, 0, 0, 0.1)',
        zIndex: 1,
        width: 2,
      },
      dateTimeLabelFormats: customConfig[configType].xDateTimeLabelFormats,
    },
    yAxis: {
      offset: 55,
      showLastLabel: true,
      labels: {
        format: '{value}',
        y: 3,
      },
    },
    plotOptions: {
      series: {
        animation: !disableAnimation,
      },
    },
  };
};

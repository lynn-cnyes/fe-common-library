import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactHighstock from 'react-highcharts/ReactHighstock';
import { InitialOptions, ChartDataFactory, ChartConfigFactory } from './ChartFactory';

const Highcharts = ReactHighstock.Highcharts;

const initHighcharts = () => {
  if (typeof window !== 'undefined' && Highcharts) {
    Highcharts.setOptions(InitialOptions);
  }
};
const buildChartConfig = ({ history, period, disableAnimation, lastClosePrice, customStyle }) => {
  const chartData = ChartDataFactory({ data: history });
  const chartConfig = ChartConfigFactory({
    configType: period,
    chartData,
    disableAnimation,
    lastClosePrice,
    customStyle,
  });

  return chartConfig;
};

class StockChart extends Component {
  static propTypes = {
    history: PropTypes.shape({
      c: PropTypes.arrayOf(PropTypes.number),
      t: PropTypes.arrayOf(PropTypes.number),
    }),
    period: PropTypes.oneOf(['24h', '3m', '1y', '3y']).isRequired,
    isLoading: PropTypes.bool,
    disableAnimation: PropTypes.bool,
    lastClosePrice: PropTypes.number,
    customStyle: PropTypes.shape({
      chartHeight: PropTypes.number,
      chartWidth: PropTypes.number,
      lineColor: PropTypes.string,
      closeLineColor: PropTypes.string,
    }),
  };

  static defaultProps = {
    history: null,
    isLoading: false,
    disableAnimation: false,
    lastClosePrice: null,
    customStyle: {},
  };

  constructor(props) {
    super(props);

    initHighcharts();

    this.state = {
      chartConfig: buildChartConfig({
        history: props.history,
        period: props.period,
        disableAnimation: props.disableAnimation,
        lastClosePrice: props.lastClosePrice,
        customStyle: props.customStyle,
      }),
    };
  }

  componentDidMount() {
    this.handleLoadingStatusChange(this.props.isLoading);
  }

  componentWillReceiveProps(nextProps) {
    const willBeLoading = nextProps.isLoading;
    const willHistoryBeChanged = this.props.history !== nextProps.history;
    const willHistoryBeSet = nextProps.history && nextProps.history.c && nextProps.history.t;

    if (willHistoryBeChanged && willHistoryBeSet && !willBeLoading) {
      this.setState({
        chartConfig: buildChartConfig(nextProps),
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoading !== this.props.isLoading) {
      this.handleLoadingStatusChange(this.props.isLoading);
    }
  }

  handleLoadingStatusChange = isLoading => {
    if (this.chart) {
      if (isLoading) {
        this.chart.showLoading();
      } else {
        this.chart.hideLoading();
      }
    }
  };

  handleAfterChartRender = chart => {
    this.chart = chart;

    // Overwriting chart.reflow is a workaround to resolve the problem that the initial animation is not working.
    // https://github.com/kirjs/react-highcharts/issues/171#issuecomment-267821262
    this.chart.reflow = () => {};
  };

  render() {
    const { chartConfig } = this.state;

    return <ReactHighstock callback={this.handleAfterChartRender} config={chartConfig} isPureConfig />;
  }
}

export default StockChart;

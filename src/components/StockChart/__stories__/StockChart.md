## Usage

```
import StockChart from 'fe-common-library/dest/components/StockChart/StockChart';
import 'fe-common-library/dest/components/StockChart/style.css';

// ...

render() {
  return (
    <StockChart
      history={data}
      period="24h"
      isLoading={isLoading}
      disableAnimation={disableAnimation}
    />
  );
}
```
/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
// import { withState } from '@dump247/storybook-state';
import * as knobs from '@storybook/addon-knobs/react';
import StockChart from '../StockChart';
import mockData from './ChartData.json';
import defaultNotes from './StockChart.md';

const stories = storiesOf('StockChart', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'Default',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const periods = ['24h', '3m', '1y', '3y'];

      return (
        <div>
          {periods.map(period => (
            <div key={period}>
              <h3>{period}</h3>
              <StockChart history={mockData[period]} period={period} />
            </div>
          ))}
        </div>
      );
    })
  )
);

stories.add(
  'lastClosePrice',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <StockChart history={mockData['24h']} period="24h" lastClosePrice={6400} />;
    })
  )
);

stories.add(
  'isLoading',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <StockChart history={mockData['24h']} period="24h" isLoading />;
    })
  )
);

stories.add(
  'disableAnimation',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <StockChart history={mockData['24h']} period="24h" disableAnimation />;
    })
  )
);

stories.add(
  'customStyle',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <StockChart
          history={mockData['24h']}
          period="24h"
          customStyle={{
            chartHeight: 300,
            chartWidth: 600,
            lineColor: 'aquamarine',
            closeLineColor: 'goldenrod',
          }}
          lastClosePrice={6400}
        />
      );
    })
  )
);

import { ChartDataFactory, ChartConfigFactory } from '../ChartFactory';

describe('ChartFactory', () => {
  describe('ChartDataFactory', () => {
    it('should transfer data to 2-dimension array', () => {
      const data = {
        c: [6432.81, 6434.57],
        t: [1541411280, 1541410920],
      };
      const result = [[1541410920 * 1000, 6434.57], [1541411280 * 1000, 6432.81]];

      expect(ChartDataFactory({ data })).toEqual(result);
    });

    it('should return null if data format is not correct', () => {
      const data = {
        c: [6432.81, 6434.57],
      };

      expect(ChartDataFactory({ data })).toBeNull();
    });
  });

  describe('ChartConfigFactory', () => {
    it('should match snapshot', () => {
      const detail = {
        configType: '24h',
        chartData: [
          [1516723200000, 10784.02],
          [1516723260000, 10820.27],
          [1516723320000, 10862.48],
          [1516723380000, 10877.1],
          [1516723440000, 10899.32],
        ],
        customStyle: {},
      };

      expect(ChartConfigFactory(detail)).toMatchSnapshot();
    });

    it('should disable animation if set disableAnimation to true', () => {
      const detail = {
        configType: '24h',
        chartData: [
          [1516723200000, 10784.02],
          [1516723260000, 10820.27],
          [1516723320000, 10862.48],
          [1516723380000, 10877.1],
          [1516723440000, 10899.32],
        ],
        customStyle: {},
        disableAnimation: true,
      };
      const result = ChartConfigFactory(detail);

      expect(result.plotOptions.series.animation).toBeFalsy();
    });

    it('should render close line if set lastClosePrice', () => {
      const lastClosePrice = 10500;
      const detail = {
        configType: '24h',
        chartData: [
          [1516723200000, 10784.02],
          [1516723260000, 10820.27],
          [1516723320000, 10862.48],
          [1516723380000, 10877.1],
          [1516723440000, 10899.32],
        ],
        customStyle: {},
        lastClosePrice,
      };
      const result = ChartConfigFactory(detail);

      expect(result.series.length).toBe(2);
      expect(result.series[1].data[0][1]).toBe(lastClosePrice);
    });

    it('should adapt custom style', () => {
      const chartHeight = 99;
      const chartWidth = 999;
      const lineColor = 'black';
      const closeLineColor = 'white';
      const lastClosePrice = 10500;
      const detail = {
        configType: '24h',
        chartData: [
          [1516723200000, 10784.02],
          [1516723260000, 10820.27],
          [1516723320000, 10862.48],
          [1516723380000, 10877.1],
          [1516723440000, 10899.32],
        ],
        customStyle: {
          chartHeight,
          chartWidth,
          lineColor,
          closeLineColor,
        },
        lastClosePrice,
      };
      const result = ChartConfigFactory(detail);

      expect(result.chart.height).toBe(chartHeight);
      expect(result.chart.width).toBe(chartWidth);
      expect(result.series[0].color).toBe(lineColor);
      expect(result.series[1].color).toBe(closeLineColor);
    });
  });
});

/* eslint-disable import/first */
jest.mock('react-highcharts/ReactHighstock');

import React from 'react';
import { mount } from 'enzyme';
import ReactHighstock from 'react-highcharts/ReactHighstock';
import StockChart from '../StockChart';
import mockData from './mocks/ChartData.json';

describe('<StockChart />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const params = {
      history: mockData,
      period: '24h',
    };

    makeSubject = (_props = {}) => {
      const props = {
        ...params,
        ..._props,
      };

      return mount(<StockChart {...props} />);
    };
  });

  it('should be fine', () => {
    const subject = makeSubject();

    expect(subject.find(ReactHighstock).length).toBe(1);
  });

  describe('handleLoadingStatusChange()', () => {
    let subject;

    beforeEach(() => {
      subject = makeSubject();
      subject.instance().chart = {
        showLoading: jest.fn(),
        hideLoading: jest.fn(),
      };
    });

    it('should show chart loading when loading', () => {
      expect(subject.instance().chart.showLoading).toHaveBeenCalledTimes(0);

      subject.instance().handleLoadingStatusChange(true);

      expect(subject.instance().chart.showLoading).toHaveBeenCalledTimes(1);
    });

    it('should hide chart loading when not loading', () => {
      expect(subject.instance().chart.hideLoading).toHaveBeenCalledTimes(0);

      subject.instance().handleLoadingStatusChange(false);

      expect(subject.instance().chart.hideLoading).toHaveBeenCalledTimes(1);
    });
  });

  describe('change props and lifecycle', () => {
    it('should update chartConfig only if [willHistoryBeChanged, willHistoryBeSet, willBeLoading] = [t, t, f]', () => {
      const subject = makeSubject();
      const nextProps = {
        isLoading: false,
        history: {
          ...mockData,
          s: '',
        },
      };

      subject.instance().setState = jest.fn();
      subject.setProps(nextProps);

      expect(subject.instance().setState).toBeCalled();
    });

    it('should not update chartConfig if nextProps.isLoading is true', () => {
      const subject = makeSubject();
      const nextProps = {
        isLoading: true,
        history: {
          ...mockData,
          s: '',
        },
      };

      subject.instance().setState = jest.fn();
      subject.setProps(nextProps);

      expect(subject.instance().setState).not.toBeCalled();
    });

    it('should not update chartConfig if nextProps.history does not change', () => {
      const subject = makeSubject();
      const nextProps = {
        isLoading: false,
        history: mockData,
      };

      subject.instance().setState = jest.fn();
      subject.setProps(nextProps);

      expect(subject.instance().setState).not.toBeCalled();
    });

    it('should not update chartConfig if nextProps.history has miss some property', () => {
      const subject = makeSubject();
      const nextProps = {
        isLoading: false,
        history: {
          ...mockData,
          c: null,
        },
      };

      subject.instance().setState = jest.fn();
      subject.setProps(nextProps);

      expect(subject.instance().setState).not.toBeCalled();
    });

    it('should work nice if props.isLoading is different from previous at componentDidUpdate', () => {
      const subject = makeSubject();
      const previousLoading = subject.state().isLoading;

      subject.instance().handleLoadingStatusChange = jest.fn();
      subject.setProps({ isLoading: !previousLoading });

      expect(subject.instance().handleLoadingStatusChange).toBeCalled();
    });

    it('should work nice if props.isLoading is the same as previous at componentDidUpdate', () => {
      const subject = makeSubject();
      const previousLoading = subject.state().isLoading;

      subject.instance().handleLoadingStatusChange = jest.fn();
      subject.setProps({ isLoading: previousLoading });

      expect(subject.instance().handleLoadingStatusChange).not.toBeCalled();
    });
  });
});

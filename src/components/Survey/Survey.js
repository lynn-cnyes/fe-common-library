import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import getStyleName from '../../utils/getStyleName';
import ls from '../../utils/localStorageWrapper';
import styles from './Survey.scss';

const EXPIRED_AFTER = 6 * 30 * 24 * 60 * 60 * 1000; // 六個月

class Survey extends PureComponent {
  static propTypes = {
    newsId: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      clicked: false,
    };

    this.removeExpiredSurveyRecords();

    // 如果 localStorage 裡有包含該新聞 ID 的 key，代表六個內使用者回答過該新聞的調查，便不再顯示調查框
    if (ls.getItem(`surveyNewsId:${this.props.newsId}`)) this.surveyed = true;
  }

  removeExpiredSurveyRecords = () => {
    const now = new Date();

    ls.filter(key => key.includes('surveyNewsId:') && now - ls.getItem(key) > EXPIRED_AFTER).forEach(ls.removeItem);
  };

  handleClick = () => {
    ls.setItem(`surveyNewsId:${this.props.newsId}`, new Date().getTime());

    this.setState({ clicked: true });
  };

  render() {
    const { clicked } = this.state;

    if (this.surveyed) return null;

    if (clicked) {
      return <div className={getStyleName(styles, 'cnyes-survey')}>感謝您的寶貴意見</div>;
    }

    const { newsId } = this.props;

    return (
      <div className={getStyleName(styles, 'cnyes-survey')}>
        <div className={getStyleName(styles, 'cnyes-survey-question')}>這篇新聞對您在投資上是否有幫助？</div>
        <div className={getStyleName(styles, 'cnyes-survey-options')}>
          <button
            data-ga-category="內容是否有助投資"
            data-ga-action="有幫助"
            data-ga-label={newsId}
            onClick={this.handleClick}
          >
            是
          </button>
          <button
            data-ga-category="內容是否有助投資"
            data-ga-action="沒幫助"
            data-ga-label={newsId}
            onClick={this.handleClick}
          >
            否
          </button>
        </div>
      </div>
    );
  }
}

export default Survey;

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import Survey from '../Survey';
import notes from './Survey.md';

storiesOf('Survey', module)
  .addDecorator(story => <div style={{ padding: '1rem' }}>{story()}</div>)
  .add(
    'default',
    withInfo()(
      withNotes(notes)(() => {
        window.localStorage.removeItem('surveyNewsId:4100137');

        return <Survey newsId={4100137} />;
      })
    )
  );

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import MobileMenu from './components/MobileHeader/MobileMenu/MobileMenu';
import MobileNavBoard from './components/MobileHeader/NavBoard/NavBoard';
import Survey from './components/Survey/Survey';
import SimpleModal from './components/SimpleModal/SimpleModal';

export { Footer, Header, MobileMenu, MobileNavBoard, Survey, SimpleModal };

/* global fail:false */
/* eslint-disable global-require, no-unexpected-multiline */
import nock from 'nock';

const APP_NAME = 'stock';
const PROJECT_NAME = 'fe-stock';

function onlyForAuth(rule, scope) {
  switch (rule) {
    case 'valid':
      scope.matchHeader('Authorization', /valid/);
      break;
    case 'invalid':
      scope.matchHeader('Authorization', /expired/);
      break;
    case 'any':
    default:
    // any
  }

  return scope;
}

describe('apiClientFactory', () => {
  let apiClientFactory;
  let axiosHttp;
  let scope;
  let scopeInvalid;
  let makeClient;

  let makeResponse;
  let hostname;
  let apiBaseUrl;
  let path;
  let requestMethod;
  let expectedResponseBody;
  let requestHeaderCollector;
  let requestWithBody;
  let requestBody;
  let requestBodyCollector;
  let requestConfig;
  let getToken;
  let refreshToken;
  let requestNeedAuth;
  let requestNeedAuthExpectedFailTimes;
  let currentToken;
  let refreshTokens;

  beforeEach(() => {
    jest.resetModules();
    nock.cleanAll();
    scope = null;
    scopeInvalid = null;
    requestNeedAuthExpectedFailTimes = 1;

    apiClientFactory = require('../apiClientFactory').default;
    axiosHttp = require('axios/lib/adapters/http');

    makeClient = (_hostname, _apiBaseUrl, extraConfig) => {
      return apiClientFactory(
        {
          baseURL: _apiBaseUrl,
          adapter: axiosHttp,
        },
        {
          ...extraConfig,
          hostname: _hostname,
          appName: APP_NAME,
          projectName: PROJECT_NAME,
        }
      );
    };
  });

  beforeEach(() => {
    hostname = 'PAAP.cnyes';
    apiBaseUrl = 'http://api.cnyes.cc';
    path = '/api/blahblah';
    requestMethod = undefined;
    expectedResponseBody = {
      dead: 'beef',
    };
    requestHeaderCollector = jest.fn();
    requestWithBody = false;
    requestBody = undefined;
    requestBodyCollector = jest.fn();
    requestConfig = {};
    requestNeedAuth = false;

    currentToken = undefined;
    refreshTokens = [];

    getToken = jest.fn(() => currentToken);
    refreshToken = jest.fn(() => {
      currentToken = refreshTokens.shift();
      // console.log('currentToken after refreshToken', currentToken);

      return Promise.resolve();
    });

    makeResponse = () => {
      const client = makeClient(hostname, apiBaseUrl, { getToken, refreshToken });

      scopeInvalid = nock(apiBaseUrl);
      scope = nock(apiBaseUrl);

      if (requestWithBody) {
        scope = onlyForAuth(requestNeedAuth ? 'valid' : 'any', scope)
          .filteringRequestBody(body => {
            requestBodyCollector(body);

            return body;
          })
          [requestMethod](path, requestBody)
          .reply(200, function replyHandler() {
            requestHeaderCollector(this.req.headers);

            return expectedResponseBody;
          });

        if (requestNeedAuth) {
          scopeInvalid = onlyForAuth('invalid', scopeInvalid)
            .filteringRequestBody(body => {
              requestBodyCollector(body);

              return body;
            })
            [requestMethod](path, requestBody)
            .times(requestNeedAuthExpectedFailTimes)
            .reply(401, function replyHandler() {
              requestHeaderCollector(this.req.headers);

              return 'Invalid Auth';
            });
        }

        return client[requestMethod](path, requestBody, requestConfig);
      }

      // prettier-ignore
      scope = onlyForAuth(requestNeedAuth ? 'valid' : 'any', scope)[requestMethod](path)
        .reply(200, function replyHandler() {
          requestHeaderCollector(this.req.headers);

          return expectedResponseBody;
        });
      // console.log('pendingMocks', scope.pendingMocks());

      if (requestNeedAuth) {
        // prettier-ignore
        scopeInvalid = onlyForAuth('invalid', scopeInvalid)[requestMethod](path)
          .times(requestNeedAuthExpectedFailTimes)
          .reply(401, function replyHandler() {
            requestHeaderCollector(this.req.headers);

            return 'Invalid Auth';
          });
      }
      // console.log('pendingMocks', scopeInvalid.pendingMocks());

      // console.log('send request!!!', requestMethod, path, requestConfig);
      return client[requestMethod](path, requestConfig);
    };
  });

  describe('on server side', () => {
    beforeEach(() => {
      global.__SERVER__ = true;
    });

    ['get', 'delete'].forEach(verb => {
      describe(`when ${verb}`, () => {
        beforeEach(() => {
          requestMethod = verb;
        });

        it('should get response body', async () => {
          const response = await makeResponse();

          expect(response.data).toEqual(expectedResponseBody);
          expect(scope.isDone()).toBeTruthy();
        });

        it('should send customized User-Agent header', async () => {
          await makeResponse();

          expect(requestHeaderCollector.mock.calls[0][0]['user-agent']).toEqual(`axios ${PROJECT_NAME} ${hostname}`);
          expect(scope.isDone()).toBeTruthy();
        });
      });
    });

    ['post', 'put', 'patch'].forEach(verb => {
      describe(`when ${verb}`, () => {
        beforeEach(() => {
          requestMethod = verb;
          requestWithBody = true;
          requestBody = {
            good: 'body',
          };
        });

        it('should get response body', async () => {
          const response = await makeResponse();

          expect(response.data).toEqual(expectedResponseBody);
          expect(scope.isDone()).toBeTruthy();
        });

        it('should send customized User-Agent header', async () => {
          await makeResponse();

          expect(requestHeaderCollector.mock.calls[0][0]['user-agent']).toEqual(`axios ${PROJECT_NAME} ${hostname}`);
          expect(scope.isDone()).toBeTruthy();
        });
      });
    });
  });

  describe('on client side', () => {
    beforeEach(() => {
      global.__SERVER__ = false;
    });

    ['get', 'delete'].forEach(verb => {
      describe(`when ${verb}`, () => {
        beforeEach(() => {
          requestMethod = verb;
        });

        describe('when not specify cnnyesauth', () => {
          it('should get response body', async () => {
            const response = await makeResponse();

            expect(response.data).toEqual(expectedResponseBody);
            expect(scope.isDone()).toBeTruthy();
          });
        });

        describe('when cnnyesauth=required', () => {
          beforeEach(() => {
            requestNeedAuth = true;
            requestConfig = {
              cnyesauth: 'required',
            };
            currentToken = 'valid lalala';
            refreshTokens = ['valid 1234', 'valid 2345', 'valid 3456'];
          });

          it('should get response body', async () => {
            const response = await makeResponse();

            expect(response.data).toEqual(expectedResponseBody);
            expect(scope.isDone()).toBeTruthy();
          });

          it('should request with Authorization header', async () => {
            await makeResponse();
            expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('valid lalala');
            expect(requestHeaderCollector.mock.calls[0][0]['x-cnyes-app']).toEqual(APP_NAME);
            expect(scope.isDone()).toBeTruthy();
          });

          describe('without auth', () => {
            beforeEach(() => {
              currentToken = undefined;
              refreshTokens = [];
            });

            it('should be rejected', async () => {
              return makeResponse()
                .then(() => fail('it should be rejected'))
                .catch(err => {
                  expect(err).toEqual(new Error('Auth is Required'));
                  expect(scope.isDone()).toBeFalsy();
                  expect(scopeInvalid.isDone()).toBeFalsy();
                });
            });
          });

          describe('with expired Authorization header', () => {
            beforeEach(() => {
              requestNeedAuth = true;
              currentToken = 'expired token 1';
            });

            it('should be rejected first with expired Authorization header', async () => {
              return makeResponse().then(() => {
                expect(scopeInvalid.pendingMocks()).toEqual([]);
                expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
              });
            });

            it('should call refreshToken and request again with new Authorization header', async () => {
              return makeResponse().then(() => {
                expect(scope.pendingMocks()).toEqual([]);
                expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
              });
            });

            it('should be refreshed and request again with expired Authorization header and new Authorization header', async () => {
              await makeResponse();

              expect(scope.isDone()).toBeTruthy();
              expect(scopeInvalid.isDone()).toBeTruthy();
              expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
              expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
            });

            it('should be rejected if refreshed and still invalid Authorization header', async () => {
              refreshTokens = ['expired by refreshToken'];
              requestNeedAuthExpectedFailTimes = 2;

              return makeResponse()
                .then(() => {
                  fail('request should be rejected');
                })
                .catch(err => {
                  expect(err).toEqual(new Error('Request failed with status code 401'));
                  expect(scope.isDone()).toBeFalsy();
                  expect(scopeInvalid.isDone()).toBeTruthy();
                  expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
                  expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('expired by refreshToken');
                });
            });
          });
        });

        describe('when cnnyesauth=optional', () => {
          beforeEach(() => {
            requestConfig = {
              cnyesauth: 'optional',
            };
            currentToken = 'valid lalala';
            refreshTokens = ['valid 1234', 'valid 2345', 'valid 3456'];
          });

          describe('without token', () => {
            beforeEach(() => {
              currentToken = undefined;
            });

            it('should get response body', async () => {
              const response = await makeResponse();

              expect(response.data).toEqual(expectedResponseBody);
              expect(scope.isDone()).toBeTruthy();
            });

            it('should request without Authorization header', async () => {
              await makeResponse();

              expect(requestHeaderCollector.mock.calls[0][0].authorization).toBeUndefined();
              expect(scope.isDone()).toBeTruthy();
            });
          });

          describe('with token', () => {
            beforeEach(() => {});

            it('should get response body', async () => {
              const response = await makeResponse();

              expect(response.data).toEqual(expectedResponseBody);
              expect(scope.isDone()).toBeTruthy();
            });

            it('should request with Authorization header', async () => {
              const expectedToken = currentToken;

              await makeResponse();

              expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual(expectedToken);
              expect(scope.isDone()).toBeTruthy();
            });
          });

          describe('with expired Authorization header', () => {
            beforeEach(() => {
              requestNeedAuth = true;
              currentToken = 'expired token 1';
            });

            it('should be rejected first with expired Authorization header', async () => {
              return makeResponse()
                .then(() => {
                  expect(scopeInvalid.pendingMocks()).toEqual([]);
                  expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
                })
                .catch(err => {
                  // console.log('pendingMocks', scopeInvalid.pendingMocks());
                  // console.log('pendingMocks', scope.pendingMocks());
                  throw err;
                });
            });

            it('should call refreshToken and request again with new Authorization header', async () => {
              return makeResponse().then(() => {
                expect(scope.pendingMocks()).toEqual([]);
                expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
              });
            });

            it('should be refreshed and request again with expired Authorization header and new Authorization header', async () => {
              await makeResponse();

              expect(scope.isDone()).toBeTruthy();
              expect(scopeInvalid.isDone()).toBeTruthy();
              expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
              expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
            });

            it('should be rejected if refreshed and still invalid Authorization header', async () => {
              refreshTokens = ['expired by refreshToken'];
              requestNeedAuthExpectedFailTimes = 2;

              return makeResponse()
                .then(() => {
                  fail('request should be rejected');
                })
                .catch(err => {
                  expect(err).toEqual(new Error('Request failed with status code 401'));
                  expect(scope.isDone()).toBeFalsy();
                  expect(scopeInvalid.isDone()).toBeTruthy();
                  expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
                  expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('expired by refreshToken');
                });
            });
          });
        });
      });
    });

    ['post', 'put', 'patch'].forEach(verb => {
      describe(`when ${verb}`, () => {
        beforeEach(() => {
          requestMethod = verb;
          requestWithBody = true;
          requestBody = {
            good: 'body',
          };
        });

        describe('when not specify cnnyesauth', () => {
          it('should get response body', async () => {
            const response = await makeResponse();

            expect(response.data).toEqual(expectedResponseBody);
            expect(scope.isDone()).toBeTruthy();
          });
        });

        describe('when cnnyesauth=required', () => {
          beforeEach(() => {
            requestNeedAuth = true;
            requestConfig = {
              cnyesauth: 'required',
            };
            currentToken = 'valid lalala';
            refreshTokens = ['valid 1234', 'valid 2345', 'valid 3456'];
          });

          it('should get response body', async () => {
            const response = await makeResponse();

            expect(response.data).toEqual(expectedResponseBody);
            expect(scope.isDone()).toBeTruthy();
          });

          it('should request with Authorization header', async () => {
            await makeResponse();

            expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('valid lalala');
            expect(scope.isDone()).toBeTruthy();
          });

          describe('without auth', () => {
            beforeEach(() => {
              currentToken = undefined;
              refreshTokens = [];
            });

            it('should be rejected', async () => {
              return makeResponse()
                .then(() => fail('it should be rejected'))
                .catch(err => {
                  expect(err).toEqual(new Error('Auth is Required'));
                  expect(scope.isDone()).toBeFalsy();
                  expect(scopeInvalid.isDone()).toBeFalsy();
                });
            });
          });

          describe('with expired Authorization header', () => {
            beforeEach(() => {
              requestNeedAuth = true;
              currentToken = 'expired token 1';
            });

            it('should be rejected first with expired Authorization header', async () => {
              return makeResponse().then(() => {
                expect(scopeInvalid.pendingMocks()).toEqual([]);
                expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
              });
            });

            it('should call refreshToken and request again with new Authorization header', async () => {
              return makeResponse().then(() => {
                expect(scope.pendingMocks()).toEqual([]);
                expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
              });
            });

            it('should be refreshed and request again with expired Authorization header and new Authorization header', async () => {
              await makeResponse();

              expect(scope.isDone()).toBeTruthy();
              expect(scopeInvalid.isDone()).toBeTruthy();
              expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
              expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
            });

            it('should be rejected if refreshed and still invalid Authorization header', async () => {
              refreshTokens = ['expired by refreshToken'];
              requestNeedAuthExpectedFailTimes = 2;

              return makeResponse()
                .then(() => {
                  fail('request should be rejected');
                })
                .catch(err => {
                  expect(err).toEqual(new Error('Request failed with status code 401'));
                  expect(scope.isDone()).toBeFalsy();
                  expect(scopeInvalid.isDone()).toBeTruthy();
                  expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
                  expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('expired by refreshToken');
                });
            });
          });
        });

        describe('when cnnyesauth=optional', () => {
          beforeEach(() => {
            requestConfig = {
              cnyesauth: 'optional',
            };
            currentToken = 'valid lalala';
            refreshTokens = ['valid 1234', 'valid 2345', 'valid 3456'];
          });

          describe('without token', () => {
            beforeEach(() => {
              currentToken = undefined;
            });

            it('should get response body', async () => {
              const response = await makeResponse();

              expect(response.data).toEqual(expectedResponseBody);
              expect(scope.isDone()).toBeTruthy();
            });

            it('should request without Authorization header', async () => {
              await makeResponse();

              expect(requestHeaderCollector.mock.calls[0][0].authorization).toBeUndefined();
              expect(scope.isDone()).toBeTruthy();
            });
          });

          describe('with token', () => {
            beforeEach(() => {});

            it('should get response body', async () => {
              const response = await makeResponse();

              expect(response.data).toEqual(expectedResponseBody);
              expect(scope.isDone()).toBeTruthy();
            });

            it('should request with Authorization header', async () => {
              const expectedToken = currentToken;

              await makeResponse();

              expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual(expectedToken);
              expect(scope.isDone()).toBeTruthy();
            });
          });

          describe('with expired Authorization header', () => {
            beforeEach(() => {
              requestNeedAuth = true;
              currentToken = 'expired token 1';
            });

            it('should be rejected first with expired Authorization header', async () => {
              return makeResponse()
                .then(() => {
                  expect(scopeInvalid.pendingMocks()).toEqual([]);
                  expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
                })
                .catch(err => {
                  // console.log('pendingMocks', scopeInvalid.pendingMocks());
                  // console.log('pendingMocks', scope.pendingMocks());
                  throw err;
                });
            });

            it('should call refreshToken and request again with new Authorization header', async () => {
              return makeResponse().then(() => {
                expect(scope.pendingMocks()).toEqual([]);
                expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
              });
            });

            it('should be refreshed and request again with expired Authorization header and new Authorization header', async () => {
              await makeResponse();

              expect(scope.isDone()).toBeTruthy();
              expect(scopeInvalid.isDone()).toBeTruthy();
              expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
              expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
              expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('valid 1234');
            });

            it('should be rejected if refreshed and still invalid Authorization header', async () => {
              refreshTokens = ['expired by refreshToken'];
              requestNeedAuthExpectedFailTimes = 2;

              return makeResponse()
                .then(() => {
                  fail('request should be rejected');
                })
                .catch(err => {
                  expect(err).toEqual(new Error('Request failed with status code 401'));
                  expect(scope.isDone()).toBeFalsy();
                  expect(scopeInvalid.isDone()).toBeTruthy();
                  expect(requestHeaderCollector.mock.calls[0]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[0][0].authorization).toEqual('expired token 1');
                  expect(requestHeaderCollector.mock.calls[1]).toBeDefined();
                  expect(requestHeaderCollector.mock.calls[1][0].authorization).toEqual('expired by refreshToken');
                });
            });
          });
        });
      });
    });
  });
});

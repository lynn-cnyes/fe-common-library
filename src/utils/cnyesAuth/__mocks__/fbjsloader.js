/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

import deepEqual from 'deep-equal';
import pubsubFactory from '../providers/__tests__/pubsubFactory';

function emptyFunc() {}

class FBClass {
  constructor() {
    // next login promise
    this._nextLoginResolve = undefined;
    this._nextAuthResponse = undefined;
    // current
    this._currentAuthResponse = { status: 'unknown' };
    // init opt
    this._opt = undefined;
  }

  _pubsub = pubsubFactory();

  init = jest.fn(opt => {
    this._opt = opt;
    if (opt.status === true) {
      this._popAuthResponse();
    }
  });

  login = jest.fn((cb = emptyFunc) => {
    if (this._nextAuthResponse) {
      cb(this._popAuthResponse());
    } else {
      new Promise(resolve => {
        this._nextLoginResolve = () => {
          resolve(this._popAuthResponse());
        };
      }).then(cb);
    }
  });

  logout = jest.fn((cb = emptyFunc) => {
    this._nextAuthResponse = {
      status: 'unknown',
    };
    cb(this._popAuthResponse());
  });

  Event = {
    subscribe: this._pubsub.subscribe,
  };

  _popAuthResponse = jest.fn(() => {
    if (this._nextAuthResponse) {
      const diff = !deepEqual(this._currentAuthResponse, this._nextAuthResponse);
      const nextStatusAlpha =
        this._currentAuthResponse.status !== this._nextAuthResponse.status && this._nextAuthResponse.status;

      this._currentAuthResponse = this._nextAuthResponse;
      this._nextAuthResponse = undefined;

      switch (nextStatusAlpha) {
        case 'connected':
          this._pubsub.publish('auth.login', this._currentAuthResponse);
          this._pubsub.publish('auth.statusChange', this._currentAuthResponse);
          break;
        case 'unknown':
        case 'not_authorized':
          this._pubsub.publish('auth.logout');
          this._pubsub.publish('auth.statusChange', this._currentAuthResponse);
          break;
        default:
        // status not changed, do nothing
      }
      if (diff) {
        this._pubsub.publish('auth.authResponseChange', this._currentAuthResponse);
      }
    }

    return this._currentAuthResponse;
  });
}

const realFB = new FBClass();

const FB = {
  init: jest.fn(realFB.init),
  login: jest.fn(realFB.login),
  logout: jest.fn(realFB.logout),
  Event: {
    subscribe: jest.fn(realFB.Event.subscribe),
  },
};

export default jest.fn(() => {
  window.FB = FB;
  window.realFB = realFB;
  if (window.__realFBOpts) {
    Object.keys(window.__realFBOpts).forEach(k => {
      window.realFB[k] = window.__realFBOpts[k];
    });
  }
  if (window.fbAsyncInit) {
    window.fbAsyncInit();
  }
});

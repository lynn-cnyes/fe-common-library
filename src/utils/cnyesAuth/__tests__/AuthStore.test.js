/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
import MockDate from 'mockdate';

describe('AuthStore', () => {
  describe('with cookie', () => {
    let authStore;
    const someday = '2019-01-31T14:00:00.000Z';
    let cookie;
    let _cookie;
    let Cookies;

    beforeEach(() => {
      jest.resetModules();

      MockDate.set(someday);
      _cookie = require.requireActual('cookie');

      const realSerialize = _cookie.serialize;

      _cookie.serialize = jest.fn((...args) => realSerialize(...args));

      Cookies = require('react-cookie').Cookies;
      cookie = new Cookies();

      const AuthStore = require('../AuthStore').default;

      authStore = new AuthStore(cookie);
    });

    describe('.saveLastLoginId', () => {
      it('should save id', () => {
        const id = '97823465324324';

        authStore.saveLastLoginId(id);
        expect(document.cookie).toBe(`cnl=${id}`);
      });

      it('should save in cookie with 7 days', () => {
        const id = '978239788967896';
        const expectedDate = new Date(+new Date(someday) + 7 * 60 * 60 * 24 * 1000);

        authStore.saveLastLoginId(id);
        expect(document.cookie).toBe(`cnl=${id}`);
        expect(_cookie.serialize).toHaveBeenCalledWith('cnl', id, { expires: expectedDate, path: '/' });
      });
    });

    describe('.loadLastLoginId', () => {
      it('should loadLastLoginId', () => {
        const id = '10209033848396525';

        authStore.saveLastLoginId(id);
        expect(authStore.loadLastLoginId()).toBe(id);
      });
    });

    describe('.destroyLastLoginId', () => {
      it('should destroy LastLoginId', () => {
        const id = '9032847253245';

        authStore.saveLastLoginId(id);
        expect(authStore.loadLastLoginId()).toBe(id);
        authStore.destroyLastLoginId();
        expect(authStore.loadLastLoginId()).toBeUndefined();
      });

      it('should destroy cnl cookie', () => {
        const id = '9032847253245';

        authStore.saveLastLoginId(id);
        expect(authStore.loadLastLoginId()).toBe(id);
        authStore.destroyLastLoginId();
        expect(document.cookie).toBe('');
      });
    });
  });
});

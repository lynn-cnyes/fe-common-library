/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

describe('cnyesAuth/providers/SimpleThenable', function() {
  let SimpleThenable;

  beforeEach(() => {
    jest.resetModules();
    SimpleThenable = require('../SimpleThenable').default;
  });

  describe('when added before resolved', () => {
    let makeSubject;

    beforeEach(() => {
      makeSubject = () => {
        const subject = jest.fn();
        const thenable = new SimpleThenable();

        thenable.then(subject);
        thenable.resolve();

        return subject;
      };
    });

    it('should be called once', () => {
      const subject = makeSubject();

      expect(subject).toHaveBeenCalledTimes(1);
    });

    it('should be called without any arguments', () => {
      const subject = makeSubject();

      expect(subject).toHaveBeenCalledWith();
    });
  });

  describe('when added after resolved', () => {
    let makeSubject;

    beforeEach(() => {
      makeSubject = () => {
        const subject1 = jest.fn();
        const subject2 = jest.fn();
        const thenable = new SimpleThenable();

        thenable.then(subject1);
        thenable.resolve();
        thenable.then(subject2);

        return [subject1, subject2];
      };
    });

    it('subject 1 should be called once', () => {
      const subject = makeSubject()[0];

      expect(subject).toHaveBeenCalledTimes(1);
    });

    it('subject 1 should be called without any arguments', () => {
      const subject = makeSubject()[0];

      expect(subject).toHaveBeenCalledWith();
    });

    it('subject 2 should be called once', () => {
      const subject = makeSubject()[1];

      expect(subject).toHaveBeenCalledTimes(1);
    });

    it('subject 2 should be called without any arguments', () => {
      const subject = makeSubject()[1];

      expect(subject).toHaveBeenCalledWith();
    });
  });
});

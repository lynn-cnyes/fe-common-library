/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

describe('auth', () => {
  let auth;

  beforeEach(() => {
    jest.resetModules();

    auth = require('../auth').default;
  });

  [
    'syncAuthWithStore',
    'init',
    'loginFB',
    'logout',
    'getToken',
    'refreshToken',
    'getProfile',
    'showLogin',
    'hideLogin',
  ].forEach(testSubject => {
    it(`should provide ${testSubject} function`, () => {
      expect(auth[testSubject]).toBeDefined();
      expect(typeof auth[testSubject]).toBe('function');
    });
  });
});

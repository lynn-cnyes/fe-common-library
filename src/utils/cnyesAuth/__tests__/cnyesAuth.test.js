/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

describe('cnyesAuth', () => {
  let cnyesAuth;
  let fbjsloader;
  let initParams;
  let PubSub;

  beforeEach(() => {
    jest.resetModules();
    jest.mock('../fbjsloader');
    cnyesAuth = require('../cnyesAuth').default;
    fbjsloader = require('../fbjsloader').default;
    PubSub = require('pubsub-js');
    window.document.head.innerHTML = '<script/>';
    window.__myInit = undefined;
    initParams = {
      awsRegion: 'ap-northeast-1',
      facebookAppId: 'fb-app-id',
      cognitoIdentityPoolId: 'identity-pool-id',
    };
  });

  describe('when facebook isnt connected', () => {
    beforeEach(() => {
      fbjsloader();
    });

    describe('init', () => {
      it('FB.init should be loaded', done => {
        window.__myInit = function() {
          expect(window.FB.init).toHaveBeenCalledTimes(1);
          expect(window.FB.init).toHaveBeenCalledWith({
            appId: 'fb-app-id',
            cookie: false,
            xfbml: false,
            version: 'v2.7',
            status: true,
          });
          done();
        };

        cnyesAuth.init(initParams);
        window.fbAsyncInit();
      });

      it('window.FBInited should be loaded', done => {
        window.__myInit = function() {
          expect(window.FB.init).toHaveBeenCalledTimes(1);
          expect(window.FB.init).toHaveBeenCalledWith({
            appId: 'fb-app-id',
            cookie: false,
            xfbml: false,
            version: 'v2.7',
            status: true,
          });
          done();
        };

        cnyesAuth.init(initParams);
        window.fbAsyncInit();
      });
    });
  });

  describe('when facebook is connected', () => {
    beforeEach(() => {
      fbjsloader();
    });

    describe('init', () => {
      it('FB.init should be loaded', done => {
        window.__myInit = function() {
          expect(window.FB.init).toHaveBeenCalledTimes(1);
          expect(window.FB.init).toHaveBeenCalledWith({
            appId: 'fb-app-id',
            cookie: false,
            xfbml: false,
            version: 'v2.7',
            status: true,
          });
          done();
        };

        cnyesAuth.init(initParams);
        window.fbAsyncInit();
      });

      it('window.FBInited should be loaded', done => {
        window.__myInit = function() {
          expect(window.FB.init).toHaveBeenCalledTimes(1);
          expect(window.FB.init).toHaveBeenCalledWith({
            appId: 'fb-app-id',
            cookie: false,
            xfbml: false,
            version: 'v2.7',
            status: true,
          });
          done();
        };

        cnyesAuth.init(initParams);
        window.fbAsyncInit();
      });

      it('aaa', done => {
        const response = {
          status: 'connected',
          authResponse: {
            userID: 'user-id',
            accessToken: 'valid fb token',
          },
        };

        cnyesAuth.init(initParams);
        window.fbAsyncInit();
        PubSub.publishSync('auth.authResponseChange', response);
        done();
      });
    });
  });

  describe('getToken', () => {
    describe('when not signed in', () => {
      it('should no token', () => {
        expect(cnyesAuth.getToken()).toBeUndefined();
      });
    });
  });
});

/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

describe('fbjsloader', () => {
  let fbjsloader;

  beforeEach(() => {
    jest.resetModules();
    // fbjsloader = require('../fbjsloader');
    const _fbjsloader = require.requireActual('../fbjsloader').default;

    fbjsloader = jest.fn(lang => _fbjsloader(lang));
    document.head.innerHTML = '<script/>';
  });

  describe('when no specify lang', () => {
    it('should load with zh_TW', () => {
      fbjsloader();
      expect(fbjsloader).toHaveBeenCalledWith();
    });

    it('should match snapshot', () => {
      fbjsloader();
      expect(document.head.innerHTML).toMatchSnapshot();
    });
  });

  describe('when zh_TW', () => {
    let lang;

    beforeEach(() => {
      lang = 'zh_TW';
    });

    it('should load with zh_TW', () => {
      fbjsloader(lang);
      expect(fbjsloader).toHaveBeenCalledWith(lang);
    });

    it('should match snapshot', () => {
      fbjsloader();
      expect(document.head.innerHTML).toMatchSnapshot();
    });
  });

  describe('when en_US', () => {
    let lang;

    beforeEach(() => {
      lang = 'en_US';
    });

    it('should load with en_US', () => {
      fbjsloader(lang);
      expect(fbjsloader).toHaveBeenCalledWith(lang);
    });

    it('should match snapshot', () => {
      fbjsloader();
      expect(document.head.innerHTML).toMatchSnapshot();
    });
  });
});

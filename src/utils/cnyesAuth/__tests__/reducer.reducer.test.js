/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
// @flow

import type { ActionType } from '../reducer';

const AUTH_INIT = 'cnyes/me/AUTH_INIT';
const AUTH_INITED = 'cnyes/me/AUTH_INITED';
const AUTH_CONNECTED = 'cnyes/me/AUTH_CONNECTED';
const AUTH_LOGOUT = 'cnyes/me/AUTH_LOGOUT';
const PROFILE_MERGE = 'cnyes/me/PROFILE_MERGE';
const PROFILE_PUT = 'cnyes/me/PROFILE_PUT';
const PROFILE_CLEAN = 'cnyes/me/PROFILE_CLEAN';

describe('cnyesAuth reducer', () => {
  let reducer;

  beforeEach(() => {
    jest.resetModules();
    reducer = require('../reducer').default;
  });

  it('should return the initial state', () => {
    const subject = reducer(undefined);

    expect(subject).toEqual({
      auth: {
        isFetching: false,
        status: 'unknown',
        token: null,
      },
      profile: undefined,
    });
  });

  describe('profile store', () => {
    let state;
    let action: ActionType | void;
    let makeSubject;

    beforeEach(() => {
      state = undefined;
      action = undefined;
      global.trackJs = {
        configure: jest.fn(),
      };
      makeSubject = () => {
        return reducer(state, action).profile;
      };
    });

    describe(`action:${PROFILE_PUT}`, () => {
      let profile;

      beforeEach(() => {
        profile = {
          uid: '98765568798',
          name: 'good boy',
          gender: 'female',
          avatar: 'http://fiewuo.ew/cewv',
          vip: 0,
        };
        action = {
          type: PROFILE_PUT,
          profile,
        };
      });

      describe('when state is empty', () => {
        beforeEach(() => {
          state = undefined;
        });

        it('should store the profile', () => {
          const subject = makeSubject();

          expect(subject).toEqual(profile);
        });

        it('should set TrackJs userId', () => {
          makeSubject();
          expect(global.trackJs.configure).toHaveBeenCalledWith({ userId: profile.uid });
          expect(global.trackJs.configure).toHaveBeenCalledTimes(1);
        });
      });

      describe('when state is not empty', () => {
        beforeEach(() => {
          state = {
            profile: {
              uid: '987tyghbi98',
              name: 'bad girl',
              avatar: 'http://fiewuo.ew/cewv',
              gender: '',
              vip: 0,
            },
          };
        });

        it('should store the new profile', () => {
          const subject = makeSubject();

          expect(subject).toEqual(profile);
        });

        it('should set TrackJs userId', () => {
          makeSubject();
          expect(global.trackJs.configure).toHaveBeenCalledWith({ userId: profile.uid });
          expect(global.trackJs.configure).toHaveBeenCalledTimes(1);
        });
      });

      describe('when state is not empty, and put same profile', () => {
        beforeEach(() => {
          state = {
            profile: {
              uid: '987tyghbi98',
              name: 'bad girl',
              avatar: 'http://fiewuo.ew/cewv',
              gender: '',
              vip: 0,
            },
          };

          profile = {
            uid: '987tyghbi98',
            name: 'bad girl',
            avatar: 'http://fiewuo.ew/cewv',
            gender: '',
            vip: 0,
          };
          if (action) {
            action.profile = profile;
          }
        });

        it('should store the new profile', () => {
          const subject = makeSubject();

          expect(subject).toEqual(profile);
        });

        it('should not set TrackJs userId', () => {
          makeSubject();
          expect(global.trackJs.configure).toHaveBeenCalledTimes(0);
        });
      });
    });

    describe(`action:${PROFILE_MERGE}`, () => {
      let profile;

      beforeEach(() => {
        profile = {
          uid: '98yguji',
          name: 'good boy',
          avatar: 'http://fiewuo.ew/cewv',
          gender: '',
          vip: 0,
        };
        action = {
          type: PROFILE_MERGE,
          profile,
        };
      });

      describe('when state is empty', () => {
        beforeEach(() => {
          state = undefined;
        });

        it('should store the profile', () => {
          const subject = makeSubject();

          expect(subject).toEqual(profile);
        });

        it('should set TrackJs userId', () => {
          makeSubject();
          expect(global.trackJs.configure).toHaveBeenCalledWith({ userId: profile.uid });
          expect(global.trackJs.configure).toHaveBeenCalledTimes(1);
        });
      });

      describe('when state is not empty', () => {
        beforeEach(() => {
          state = {
            profile: {
              uid: '98yhuijogb',
              name: 'bad girl',
              gender: 'female',
              avatar: 'http://fiewuo.ew/cewv',
              vip: 0,
            },
          };
        });

        it('should store the new profile', () => {
          const subject = makeSubject();

          expect(subject).toEqual({
            uid: '98yguji',
            name: 'good boy',
            avatar: 'http://fiewuo.ew/cewv',
            gender: '',
            vip: 0,
          });
        });

        it('should set TrackJs userId', () => {
          makeSubject();
          expect(global.trackJs.configure).toHaveBeenCalledWith({ userId: profile.uid });
          expect(global.trackJs.configure).toHaveBeenCalledTimes(1);
        });
      });
    });

    [PROFILE_CLEAN, AUTH_LOGOUT].forEach(testSubject => {
      describe(`action:${testSubject}`, () => {
        beforeEach(() => {
          state = {
            profile: {
              uid: '29hfnh89o',
              name: 'david',
              avatar: 'http://fiewuo.ew/cewv',
              gender: '',
              vip: 0,
            },
          };
          action = {
            type: testSubject,
          };
        });

        it('should reset the profile', () => {
          const subject = makeSubject();

          expect(subject).toEqual(undefined);
        });

        it('should set TrackJs userId', () => {
          makeSubject();
          expect(global.trackJs.configure).toHaveBeenCalledWith({ userId: undefined });
          expect(global.trackJs.configure).toHaveBeenCalledTimes(1);
        });
      });
    });
  });

  describe('auth store', () => {
    let state;
    let action;
    let makeSubject;

    beforeEach(() => {
      state = undefined;
      action = undefined;
      makeSubject = () => {
        return reducer(state, action).auth;
      };
    });

    describe(`action:${AUTH_INIT}`, () => {
      beforeEach(() => {
        state = {
          auth: {
            isFetching: false,
            status: 'unknown',
            token: null,
          },
        };
        action = {
          type: AUTH_INIT,
        };
      });

      it('should set isFetching:true', () => {
        const subject = makeSubject();

        expect(subject).toEqual({
          isFetching: true,
          status: 'unknown',
          token: null,
        });
      });
    });

    describe(`action:${AUTH_INITED}`, () => {
      beforeEach(() => {
        state = {
          auth: {
            isFetching: true,
            status: 'unknown',
            token: null,
          },
        };
        action = {
          type: AUTH_INITED,
        };
      });

      it('should set isFetching:false', () => {
        const subject = makeSubject();

        expect(subject).toEqual({
          isFetching: false,
          status: 'unknown',
          token: null,
        });
      });
    });

    describe(`action:${AUTH_CONNECTED}`, () => {
      beforeEach(() => {
        state = {
          auth: {
            isFetching: true,
            status: 'unknown',
            token: null,
          },
        };
        action = {
          type: AUTH_CONNECTED,
          token: 'token',
        };
      });

      it('should set isFetching:false, token and status', () => {
        const subject = makeSubject();

        expect(subject).toEqual({
          isFetching: false,
          status: 'connected',
          token: 'token',
        });
      });
    });

    describe(`action:${AUTH_LOGOUT}`, () => {
      beforeEach(() => {
        state = {
          auth: {
            isFetching: false,
            status: 'connected',
            token: 'token',
          },
        };
        action = {
          type: AUTH_LOGOUT,
        };
      });

      it('should set isFetching:false and status, remove token', () => {
        const subject = makeSubject();

        expect(subject).toEqual({
          isFetching: false,
          status: 'unknown',
          token: null,
        });
      });
    });
  });
});

import 'aws-sdk/lib/node_loader';
import AWS from 'aws-sdk/lib/core';
import 'aws-sdk/clients/cognitoidentity';
import 'aws-sdk/lib/credentials/cognito_identity_credentials';

export default AWS;

// @flow
import PubSub from 'pubsub-js';
import { Cookies } from 'react-cookie';
import type { $AxiosXHR } from 'axios';
import loadScript from 'load-script';
import apiClientFactory from '../apiClientFactory';
import AuthStore from './AuthStore';
// import aws-sdk modules
import AWS from './aws-sdk';
import FacebookProvider from './providers/facebook';
import GoogleProvider from './providers/google';

import type { ProviderType, LoginInfo, AuthResponseType, ProfileType, EventTopicType, EventMessageType } from './types';

const { CognitoIdentityCredentials, CognitoIdentity } = AWS;

export type initParamsType = {
  awsRegion: string,
  facebookAppId: string,
  googleAppId: string,
  googleAppKey: string,
  cognitoIdentityPoolId: string,
};

type cognitoTokenType = {
  IdentityId: string,
  Token: string,
};

type initOptionsType = {
  injectSdk: boolean,
};

const providers: ProviderType[] = [];

let lastLoginId: string = '';

let authConfig: initParamsType;
let currentAuthResponse: AuthResponseType = { status: 'unknown' };
let currentLoginMethod: ?string;

let getProfileApi: string;
let registrationApi: string;

const APP_NAME = 'stock';
const PROJECT_NAME = 'fe-stock';

const cookie = new Cookies();
const authStore = new AuthStore(cookie);

lastLoginId = authStore.loadLastLoginId();

function _setLastLoginId(id: string): void {
  lastLoginId = id;
  providers.forEach(i => i.setLastLoginId(lastLoginId));
}

function publishEvent(topic: EventTopicType, message?: ?EventMessageType): void {
  PubSub.publish(topic, message);
}

function _removeInfo(): void {
  currentLoginMethod = undefined;
  currentAuthResponse = { status: 'unknown' };
  _setLastLoginId('');
  authStore.destroyLastLoginId();
}

function getToken(): ?string {
  return (currentAuthResponse && currentAuthResponse.status === 'connected' && currentAuthResponse.token) || undefined;
}

function getAuthResponse(): AuthResponseType {
  return currentAuthResponse;
}

function getProfile(): Promise<ProfileType> {
  // eslint-disable-next-line no-use-before-define
  return client.get(getProfileApi, { cnyesauth: 'required' }).then((response): ProfileType => {
    const profile = (response && response.data && response.data.items) || {};

    if (profile.email && typeof window !== 'undefined' && window._paq !== undefined) {
      window._paq.push(['setUserId', profile.email]);
    }

    if (profile.uid && typeof window !== 'undefined' && window.ga !== undefined) {
      window.ga('set', 'userId', profile.uid);
    }

    publishEvent('profile.change', profile);

    return profile;
  });
}

function currentProvider(): ?ProviderType {
  switch (currentLoginMethod) {
    case 'graph.facebook.com':
      return FacebookProvider;
    case 'accounts.google.com':
      return GoogleProvider;
    default:
      return null;
  }
}

function registration(): Promise<$AxiosXHR<ProfileType>> {
  let payload;
  const provider = currentProvider();

  if (provider) {
    const info = provider.getCurrentLoginInfo();

    if (!info || !info.credentials) {
      return Promise.reject(`${currentLoginMethod || ''} not login, unable to register`);
    }

    payload = {
      token: info.temp || info.credentials, // XXX remove info.temp, this is for google token to register
    };
  }

  if (payload) {
    // eslint-disable-next-line no-use-before-define
    return client.post(registrationApi, payload, { cnyesauth: 'required' }).then((response): ProfileType => {
      const profile = (response && response.data && response.data.items) || {};

      publishEvent('profile.change', profile);

      return profile;
    });
  }

  return Promise.reject();
}

function _retriveCognitoOpenId(userID: string, firsttime: boolean = false): Promise<AuthResponseType> {
  return new Promise((resolve, reject) => {
    const params = AWS.config.credentials.params;
    const cognitoIdentity = new CognitoIdentity({ params });
    const loginMethod = params && params.Logins && Object.keys(params.Logins)[0];

    cognitoIdentity.getOpenIdToken((err, token: ?cognitoTokenType) => {
      if (err || !token) {
        reject(err);

        return;
      }

      const cToken = token.Token;

      currentLoginMethod = loginMethod;
      currentAuthResponse = {
        status: 'connected',
        token: cToken,
      };

      let _promise;

      if (firsttime) {
        _promise = getProfile().then(undefined, registration);
      } else {
        _promise = Promise.resolve();
      }

      _promise
        .then(
          () => {
            _setLastLoginId(`${loginMethod}:${userID}`);
            authStore.saveLastLoginId(lastLoginId);
            publishEvent('auth.authResponseChange', currentAuthResponse);
            publishEvent('auth.statusChange', currentAuthResponse);
            publishEvent('auth.login', cToken);
          },
          () => {
            _removeInfo();
          }
        )
        .then(() => resolve(currentAuthResponse));
    });
  });
}

/**
 * reject when failed
 */
function onLogin(info: LoginInfo): Promise<AuthResponseType> {
  let needPublishAuthEvent = false;

  const _promise = new Promise((resolve, reject): void => {
    if (currentLoginMethod === info.provider) {
      // already login, do renew token
      AWS.config.credentials.params.Logins = { [info.provider]: info.credentials };
      const userID = info.userID;

      AWS.config.credentials.refresh(err => {
        if (err) {
          reject(err);

          return;
        }

        _retriveCognitoOpenId(userID).then(resolve, reject);
      });

      return;
    } else if (currentLoginMethod) {
      reject(`${info.provider}: already login via other provider`);

      return;
    }

    if (getAuthResponse().status !== 'connected') {
      needPublishAuthEvent = true;
      publishEvent('auth.init');
    }

    AWS.config.region = authConfig.awsRegion;
    if (!AWS.config.credentials) {
      AWS.config.credentials = new CognitoIdentityCredentials({
        IdentityPoolId: authConfig.cognitoIdentityPoolId,
      });
    }
    AWS.config.credentials.params.Logins = { [info.provider]: info.credentials };
    const userID = info.userID;

    AWS.config.credentials.get(err => {
      if (err) {
        reject(err);

        return;
      }

      _retriveCognitoOpenId(userID, true).then(resolve, reject);
    });
  });

  _promise.then(
    () => {
      if (needPublishAuthEvent) publishEvent('auth.inited');
    },
    () => {
      if (needPublishAuthEvent) publishEvent('auth.inited');
    }
  );

  return _promise;
}

function refreshToken(): Promise<AuthResponseType> {
  // TODO use providers array
  switch (currentLoginMethod) {
    case 'graph.facebook.com':
      return FacebookProvider.handleLogin().catch(() => currentAuthResponse);
    case 'accounts.google.com':
      return GoogleProvider.handleLogin().catch(() => currentAuthResponse);
    default:
      return Promise.reject('not loggin or not supported provider');
  }
}

const client = apiClientFactory(
  {},
  {
    getToken,
    refreshToken,
    appName: APP_NAME,
    projectName: PROJECT_NAME,
  }
);

function logout(): void {
  const provider = currentProvider();

  if (provider) {
    provider.logout();
  }

  _removeInfo();
  if (AWS.config.credentials) {
    AWS.config.credentials.clearCachedId();
    AWS.config.region = authConfig.awsRegion;
    AWS.config.credentials = new CognitoIdentityCredentials({
      IdentityPoolId: authConfig.cognitoIdentityPoolId,
    });
  }

  publishEvent('auth.authResponseChange', {
    status: 'unknown',
  });
  publishEvent('auth.statusChange', {
    status: 'unknown',
  });
  publishEvent('auth.logout');
}

/**
 *
 * params = {
 *   awsRegion, // AWS region
 *   cognitoIdentityPoolId, // AWS identity pool id
 *   facebookAppId, // facebook app id
 *   googleAppId, // google app id
 *   googleAppKey, // google app key
 * };
 */
function init(params: initParamsType, options: initOptionsType): void {
  const injectSdk = options && options.injectSdk;

  getProfileApi = (options && options.getProfileApi) || '/api/v1/user/profile';
  registrationApi = (options && options.registrationApi) || '/api/v2/user/registration';
  authConfig = params;

  AWS.config.update({
    region: authConfig.awsRegion,
  });

  const authCore = {
    onLogin,
    getAuthResponse,
    doLogout: logout,
    publishEvent,
  };

  if (params.facebookAppId) {
    providers.push(FacebookProvider);
    FacebookProvider.init({
      appId: params.facebookAppId,
      lastLoginId,
      authCore,
    });
    if (injectSdk) loadScript('https://connect.facebook.net/zh_TW/sdk.js?c=1#xfbml=1&version=v2.8');
  }

  if (params.googleAppId && params.googleAppKey) {
    providers.push(GoogleProvider);
    GoogleProvider.init({
      appId: params.googleAppId,
      appKey: params.googleAppKey,
      lastLoginId,
      authCore,
    });
    if (injectSdk) loadScript('https://apis.google.com/js/auth2.js?onload=gAsyncInit', { attrs: { defer: true } });
  }
}

function loginFB(): Promise<AuthResponseType> {
  return FacebookProvider.login();
}

function loginGoogle(): Promise<AuthResponseType> {
  return GoogleProvider.login();
}

/**
 * TODO not implemented yet
 */
function showLogin(): void {}

/**
 * TODO not implemented yet
 */
function hideLogin(): void {}

export default {
  init,
  loginFB,
  loginGoogle,
  logout,
  Event: {
    subscribe: PubSub.subscribe,
    unsubscribe: PubSub.unsubscribe,
  },
  showLogin,
  hideLogin,
  getToken,
  getAuthResponse,
  refreshToken,
  getProfile,
  registration,
};

// }

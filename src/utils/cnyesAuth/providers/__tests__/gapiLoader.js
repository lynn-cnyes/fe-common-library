/* eslint-disable class-methods-use-this */
// @flow

import type { GoogleAuthType, GoogleApiAuth2InitParam } from '../googleType';

import pubsubFactory from './pubsubFactory';

type GoogleApi = {
  auth2: {
    init(params: GoogleApiAuth2InitParam): GoogleAuthType,
    getAuthInstance(): GoogleAuthType,
  },
};

const pubsub = pubsubFactory();

//
// window.__gapiBehaviors
//   .initSignin: true  // should be signedin when init?
//   .signin: true // should signin return success?
//   .nextAuthResponse: object
//     .access_token
//     .expires_at
//     .expires_in: 3600
//     .first_issued_at
//     .id_token
//     .idpId
//     .login_hint
//     .scope
//     .session_state
//       .extraQueryParams
//         .authuser
//     .token_type: 'Bearer
//   .nextBasicProfile: object
//     .id
//     .imageUrl
//     .familyName
//     .givenName
//     .name
//     .email
//

class GoogleBasicProfileClass {
  constructor(basicProfile) {
    this.basicProfile = basicProfile;
  }

  basicProfile = {};

  getId() {
    return this.basicProfile.id;
  }

  getName() {
    return this.basicProfile.name;
  }

  getGivenName() {
    return this.basicProfile.givenName;
  }

  getFamilyName() {
    return this.basicProfile.familyName;
  }

  getImageUrl() {
    return this.basicProfile.imageUrl;
  }

  getEmail() {
    return this.basicProfile.email;
  }
}

class GoogleUserClass {
  constructor(basicProfile, authResponse) {
    this.basicProfile = basicProfile ? new GoogleBasicProfileClass(basicProfile) : undefined;
    this.authResponse = authResponse;
  }

  basicProfile = undefined;
  authResponse = undefined;

  getId() {
    return (this.basicProfile && this.basicProfile.getId()) || null;
  }

  isSignedIn() {
    return !!this.authResponse;
  }

  getBasicProfile() {
    return this.basicProfile;
  }

  getHostedDomain() {
    throw new Error('not implemented');
  }

  getGrantedScopes() {
    throw new Error('not implemented');
  }

  getAuthResponse() {
    return this.authResponse;
  }

  reloadAuthResponse() {
    // TODO
    throw new Error('not implemented');
  }

  hasGrantedScope() {
    throw new Error('not implemented');
  }

  grant() {
    throw new Error('not implemented');
  }

  grantOfflineAccess() {
    throw new Error('not implemented');
  }

  disconnect() {
    throw new Error('not implemented');
  }
}

class GoogleAuthClass {
  _callbacks = [];
  _initialized = false;
  _fireCallback = () => {
    this._initialized = true;
    this._callbacks.forEach(cb => {
      setTimeout(() => {
        cb(this);
      }, 0);
    });
  };

  _done = () => {
    if (window.__gapiBehaviors && window.__gapiBehaviors.initSignin) {
      this._currentUser = new GoogleUserClass(
        window.__gapiBehaviors.nextBasicProfile,
        window.__gapiBehaviors.nextAuthResponse
      );

      pubsub.publish('auth2.user');
      pubsub.publish('auth2.isSignedIn');
    } else {
      this._currentUser = new GoogleUserClass();
    }

    this._fireCallback();
  };

  then = cb => {
    if (this._initialized) {
      return new Promise(resolve => {
        resolve(cb(this));
      });
    }

    return new Promise(resolve => {
      this._callbacks.push(a => {
        resolve(cb(a));
      });
    });
  };

  _currentUser = undefined;

  currentUser = {
    get: () => {
      return this._currentUser;
    },
    listen: fn => {
      pubsub.subscribe('auth2.user', fn);
    },
  };

  isSignedIn = {
    get: () => {
      return (this._currentUser && this._currentUser.isSignedIn()) || false;
    },
    listen: fn => {
      pubsub.subscribe('auth2.isSignedIn', fn);
    },
  };

  signIn() {
    return new Promise(resolve => {
      if (window.__gapiBehaviors && window.__gapiBehaviors.signIn) {
        this._currentUser = new GoogleUserClass(
          window.__gapiBehaviors.nextBasicProfile,
          window.__gapiBehaviors.nextAuthResponse
        );

        pubsub.publish('auth2.user');
        pubsub.publish('auth2.isSignedIn');

        resolve(); // yes , google auth lib only resolve when user signin... :(
      }
    });
  }

  signOut() {
    return new Promise(resolve => {
      this._currentUser = new GoogleUserClass();
      pubsub.publish('auth2.user');
      pubsub.publish('auth2.isSignedIn');

      resolve();
    });
  }

  disconnect() {
    throw new Error('not implemented yet');
  }

  // eslint-disable-next-line no-unused-vars
  grantOfflineAccess(options) {
    return new Promise(() => {
      throw new Error('not implemented yet');
    });
  }

  // eslint-disable-next-line no-unused-vars
  attachClickHandler(container, options, onsuccess, onfailure) {
    throw new Error('not implemented yet');
  }
}

let currentAuthInstance;

const gapi: GoogleApi = {
  auth2: {
    init: jest.fn(() => {
      return new Promise(resolve => {
        currentAuthInstance = new GoogleAuthClass();
        currentAuthInstance._done();
        resolve();
      });
    }),
    getAuthInstance() {
      return currentAuthInstance;
    },
  },
};

export default jest.fn(initFuncName => {
  window.gapi = {};

  setTimeout(() => {
    // gapi is inited after sometime
    window.gapi = gapi;
    if (initFuncName && window[initFuncName]) {
      window[initFuncName]();
    }
  }, 10);
});

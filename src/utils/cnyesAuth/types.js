// @flow

export type LoginInfo = {
  provider: 'graph.facebook.com' | 'accounts.google.com',
  credentials: string,
  userID: string,
};

export type AuthResponseType =
  | { status: 'connected', token: string }
  | { status: 'unknown' }
  | { status: 'not_authorized' };

export type ProfileType = {
  uid: string,
  name: string,
  nickname?: string,
  email?: string,
  avatar: string,
  gender: '' | 'male' | 'female',
  vip: 0 | 1,
};

export type EventTopicType =
  | 'auth.init'
  | 'auth.inited'
  | 'auth.authResponseChange'
  | 'auth.statusChange'
  | 'auth.login'
  | 'auth.logout'
  | 'profile.change';

export type EventMessageType = AuthResponseType | ProfileType | ?string;

export type OnLoginFunctionType = (info: LoginInfo) => Promise<AuthResponseType>;
export type DoLogoutFunctionType = () => void;
export type GetAuthResponseType = () => AuthResponseType;
export type PublishEventFunctionType = (topic: EventTopicType, message?: EventMessageType) => void;

export type AuthCoreType = {
  onLogin: OnLoginFunctionType,
  doLogout: DoLogoutFunctionType,
  getAuthResponse: GetAuthResponseType,
  publishEvent: PublishEventFunctionType,
};

export type ProviderInitParamsType = {
  appId: string,
  appKey?: string,
  lastLoginId: string,
  authCore: AuthCoreType,
};

export type ProviderType = {
  init: (params: ProviderInitParamsType) => void,
  login: () => Promise<AuthResponseType>,
  logout: () => void,
  handleLogin: () => Promise<AuthResponseType>,
  setLastLoginId: (id: string) => void,
  getCurrentLoginInfo: () => ?LoginInfo,
};

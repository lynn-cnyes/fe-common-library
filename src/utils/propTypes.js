/* eslint-disable import/prefer-default-export */
import PropTypes from 'prop-types';

export const requestType = PropTypes.func;

export const navUrlShape = PropTypes.shape({
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
});

export const navItemShape = PropTypes.shape({
  title: PropTypes.string.isRequired,
  url: PropTypes.string,
  catSlug: PropTypes.string,
  leftList: PropTypes.arrayOf(navUrlShape),
  rightListTitle: PropTypes.string,
  rightList: PropTypes.arrayOf(navUrlShape),
});

export const navsType = PropTypes.arrayOf(navItemShape);

export const catNavSubItemShape = PropTypes.shape({
  name: PropTypes.string,
  url: PropTypes.string,
  title: PropTypes.string,
  external: PropTypes.bool,
});

export const catNavItemShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  external: PropTypes.bool,
  subItems: PropTypes.arrayOf(catNavSubItemShape),
});

export const catNavsType = PropTypes.arrayOf(catNavItemShape);

export const locationShape = PropTypes.shape({
  key: PropTypes.string,
  pathname: PropTypes.string,
  search: PropTypes.string,
  hash: PropTypes.string,
  state: PropTypes.object,
});

export const authType = PropTypes.shape({
  init: PropTypes.func.isRequired,
  loginFB: PropTypes.func.isRequired,
  loginGoogle: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  showLogin: PropTypes.func.isRequired,
  hideLogin: PropTypes.func.isRequired,
  getToken: PropTypes.func.isRequired,
  refreshToken: PropTypes.func.isRequired,
  getProfile: PropTypes.func.isRequired,
});

export const userProfileType = PropTypes.shape({
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  nickname: PropTypes.string,
  email: PropTypes.string,
  avatar: PropTypes.string.isRequired,
  gender: PropTypes.oneOf(['', 'male', 'female']),
  vip: PropTypes.oneOf([0, 1]),
});

require('babel-polyfill');
require('dotenv').config();

// Webpack config for creating the production bundle.
var path = require('path');
var webpack = require('webpack');
var CleanPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
var strip = require('strip-loader');
var S3Plugin = require('webpack-s3-plugin');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

var projectRootPath = path.resolve(__dirname);
var assetsPath = path.resolve(projectRootPath, './dest');
var assetsPublicPath = process.env.ASSETS_PUBLIC_PATH;
var AWS_S3_REGION = process.env.AWS_S3_REGION;
var AWS_S3_BUCKET = process.env.AWS_S3_BUCKET;

var ENABLE_S3 = assetsPublicPath && AWS_S3_BUCKET && AWS_S3_REGION;
var assetsLoader = ENABLE_S3
  ? `file-loader?publicPath=${assetsPublicPath}`
  : 'base64-inline-loader?limit=50000&name=[name].[ext]';
var s3Options = {
  region: AWS_S3_REGION,
};
if (process.env.AWS_ACCESS_KEY_ID) {
  s3Options.accessKeyId = process.env.AWS_ACCESS_KEY_ID;
}
if (process.env.AWS_SECRET_ACCESS_KEY) {
  s3Options.secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
}

// https://github.com/halt-hammerzeit/webpack-isomorphic-tools
var WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
var webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(require('./webpack-isomorphic-tools'));

function EMPTY_PLUGIN() {}
EMPTY_PLUGIN.prototype.apply = function(compiler) {};

module.exports = {
  context: path.resolve(__dirname),
  entry: {
    lib: ['./src/index.js'],
    'components/Header': ['./src/components/Header/Header.js'],
    'components/Footer': ['./src/components/Footer/Footer.js'],
    'components/MobileHeader': ['./src/components/MobileHeader/index.js'],
    'components/Survey': ['./src/components/Survey/Survey.js'],
    'components/LoginModal': ['./src/components/LoginModal/LoginModal.js'],
    'components/CnyesProvider': ['./src/components/CnyesProvider/CnyesProvider.js'],
    'components/SimpleModal': ['./src/components/SimpleModal/SimpleModal.js'],
    'components/Modal': ['./src/components/Modal/Modal.js'],
    'components/StockChart': ['./src/components/StockChart/StockChart.js'],
    'components/SmartTime': ['./src/components/SmartTime/SmartTime.js'],
    'components/List': ['./src/components/List/index.js'],
    'utils/cnyesAuth': ['./src/utils/cnyesAuth/index.js'],
    'utils/apiClientFactory': ['./src/utils/apiClientFactory.js'],
    'utils/localStorageWrapper': ['./src/utils/localStorageWrapper.js'],
  },
  externals: ['react', 'react-dom', 'classnames', 'prop-types', 'axios', 'react-cookie', 'redux'],
  output: {
    path: assetsPath,
    filename: '[name]/index.js',
    publicPath: '/dest/',
    libraryTarget: 'umd',
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, exclude: /node_modules/, loaders: [strip.loader('debug'), 'babel-loader'] },
      { test: /\.json$/, loader: 'json-loader' },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use:
            'css-loader?-autoprefixer&modules&importLoaders=2&localIdentName=_[hash:base64:5]!postcss-loader!sass-loader?outputStyle=expanded',
        }),
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader?-autoprefixer&importLoaders=1!postcss-loader'),
      },
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        exclude: /\.inline\.svg$/,
        loader: assetsLoader,
      },
    ],
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.scss', '.json', '.jsx'],
  },
  plugins: [
    new CleanPlugin([assetsPath], { root: projectRootPath }),

    // css files from the extract-text-plugin loader
    new ExtractTextPlugin('[name]/style.css', { allChunks: true }),
    ENABLE_S3
      ? new S3Plugin({
          // Exclude uploading of html
          include: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
          // s3Options are required
          s3Options: s3Options,
          s3UploadOptions: {
            Bucket: AWS_S3_BUCKET,
          },
          basePath: process.env.ASSETS_PREFIX || '',
        })
      : EMPTY_PLUGIN,
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },

      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: false,
      __DEVTOOLS__: false,
    }),

    // ignore dev config
    new webpack.IgnorePlugin(/\.\/dev/, /\/config$/),

    // optimizations
    new webpack.optimize.OccurrenceOrderPlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     warnings: false,
    //   },
    // }),

    webpackIsomorphicToolsPlugin,
    new webpack.ContextReplacementPlugin(/moment[\\/]locale$/, /^\.\/(en|zh-tw)$/),
    process.env.ANALYZE === 'true' ? new BundleAnalyzerPlugin() : EMPTY_PLUGIN,
  ],
  node: {
    fs: 'empty',
  },
};
